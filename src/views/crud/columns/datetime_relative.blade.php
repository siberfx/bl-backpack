{{-- localized datetime using jenssegers/date --}}
<span data-order="{{ $entry->{$column['name']} }}">
    {{ \Carbon\Carbon::createFromTimeStamp(strtotime($entry->{$column['name']}))->diffForHumans() }}
</span>