{{-- regular object attribute --}}
<span style="align-content: center; text-align: center">
    <form action="/{{ $crud->route }}/{{ $entry->id }}/toggle" style="">
        <input type="hidden" name="attribute" value="{{ $column['name'] }}"/>
        @if(isset($column['default_checkbox']) && $column['default_checkbox'])
            <input type="hidden" name="default_checkbox" value="1"/>
        @endif
        <label class="switch switch-sm switch-label switch-outline-success" style="margin: 3px">
            <input class="switch-input" type="checkbox"
                  name="{{ $column['name'] }}" {{ $entry->{$column['name']} ? 'checked' : '' }} onchange="submit()"
                    data-toggle="tooltip"
                  @if(isset($column['default_checkbox']) && $column['default_checkbox'] && $entry->{$column['name']})
                    disabled
                  @endif
            ><span class="switch-slider" data-checked="✓" data-unchecked="✕"
                  @if(isset($column['default_checkbox']) && $column['default_checkbox'] && $entry->{$column['name']})
                       title="Click to enable"
                  @else
                       title="Click to disable"
                  @endif
            ></span>
        </label>
    </form>
</span>
