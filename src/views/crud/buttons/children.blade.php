@if($children = $crud->getChildrenButtons())
    @foreach($children['inline'] as $item)
        <a href="{{ url($crud->route.'/'.$entry->id.'/'.$item['basename']) }}" class="btn btn-sm btn-link"><i
                    class="la {{ $item['icon'] ?? '' }}"></i> {{ $item['label'] }}
        </a>
    @endforeach
    @if($children['dropdown'])
        <a class="nav-link nav-link-dropdown" style="display: inline;" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
           aria-expanded="false">
            <i class="la la-lg la-ellipsis-h"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right">
            @foreach($children['dropdown'] as $item)
                <a href="{{ url($crud->route.'/'.$entry->id.'/'.$item['basename']) }}"
                   class="btn dropdown-item btn-sm btn-link"><i
                            class="la {{ $item['icon'] ?? '' }}"></i> {{ $item['label'] }}
                </a>
            @endforeach
        </div>
    @endif
@endif
