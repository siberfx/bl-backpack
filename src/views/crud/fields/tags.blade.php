@include('crud::fields.inc.wrapper_start')
    <label>{!! $field['label'] !!}</label>
    <select data-type="tags" name="{{ $field['name'] }}[]" class="form-control" multiple="multiple">
        @if(isset($entry) && $field['value'])
            @foreach($field['value'] as $tag)
                <option selected="selected">{{ $tag }}</option>
            @endforeach
        @endif
    </select>
    {{-- HINT --}}
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
</div>
