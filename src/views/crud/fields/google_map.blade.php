@include('crud::fields.inc.wrapper_start')
<input id="pac-input" class="controls" type="text" placeholder="Search Box">
<div id="map" style="height: 500px"></div>
@include('crud::fields.inc.wrapper_end')
@push('after_scripts')
    <script src="https://maps.googleapis.com/maps/api/js?v=weekly&key={{ config('bl-backpack.google_api_key') }}&libraries=places"></script>
    <script src="{{ url('/bl-backpack/js/address_setter.js') }}"></script>
    <script>
        $(function() {
            init_map('{{ $field['map']['latitude'] ?? 0 }}','{{ $field['map']['longitude'] ?? 0 }}',
                $('#map'),
                $('#pac-input'),
                $('input[name={{ $field['target']['latitude'] ?? 'latitude' }}]'),
                $('input[name={{ $field['target']['longitude'] ?? 'longitude' }}]'),
                $(':input[name={{ $field['target']['address'] ?? 'address' }}]'),
                {{ $field['map']['zoom'] ?? 14 }}
            );
            $(document).on('keyup keypress', 'form input[type="text"]', function(e) {
                if(e.which == 13) {
                    e.preventDefault();
                    return false;
                }
            });
        });
    </script>
@endpush
@push('after_styles')
    <link rel="stylesheet" type="text/css" href="{{ url('/bl-backpack/css/google_map_locator.css') }}">
@endpush
