<!-- password -->

@php
    // autocomplete off, if not otherwise specified
    if (!isset($field['attributes']['autocomplete'])) {
        $field['attributes']['autocomplete'] = "off";
    }
@endphp

@include('crud::fields.inc.wrapper_start')
<label>{!! $field['label'] !!}</label>
@include('crud::fields.inc.translatable_icon')
@if(isset($field['value']))
    <div style="float: right"><label style="font-weight: normal"> <input type="checkbox" id="change_{{ $field['name'] }}" name="change_{{ $field['name'] }}" value="1" /> Change Password?</label></div>
@else
    <input type="hidden" name="change_{{ $field['name'] }}" value="1" id="change_{{ $field['name'] }}" />
@endif
<input
        type="password"
        name="{{ $field['name'] }}"
        id="{{ $field['name'] }}"
        @include('crud::fields.inc.attributes')
        disabled
>

{{-- HINT --}}
@if (isset($field['hint']))
    <p class="help-block">{!! $field['hint'] !!}</p>
@endif

@include('crud::fields.inc.wrapper_end')

{{-- Confirmation --}}
@include('crud::fields.inc.wrapper_start')
<label>Confirm {{ $field['label'] }}</label>
@include('crud::fields.inc.translatable_icon')
<input
        type="password"
        id="{{ $field['name'] }}_confirmation"
        name="{{ $field['name'] }}_confirmation"
        @include('crud::fields.inc.attributes')
        disabled
>
@include('crud::fields.inc.wrapper_end')

{{--
<!-- password -->
@include('crud::fields.inc.wrapper_start')
    <label>{!! $field['label'] !!}</label>
    @include('crud::fields.inc.translatable_icon')
    @if(isset($field['value']))
        <div style="float: right"><label style="font-weight: normal"> <input type="checkbox" id="change_{{ $field['name'] }}" name="change_{{ $field['name'] }}" value="1" /> Change Password?</label></div>
    @else
        <input type="hidden" name="change_{{ $field['name'] }}" value="1" id="change_{{ $field['name'] }}" />
        @if (isset($field['hint']))
            <div style="float: right"><span class="help-block" style="margin-bottom: 5px; margin-top: 0px" >{!! $field['hint'] !!}</span></div>
        @endif
    @endif
    <span>
    <input
            type="password"
            name="{{ $field['name'] }}"
            id="{{ $field['name'] }}"
            disabled
    >
        <label>{{ trans('baseline.confirm') }} {{ $field['label'] }}</label>
    <input
            type="password"
            id="{{ $field['name'] }}_confirmation"
            name="{{ $field['name'] }}_confirmation"
            disabled
    >

@include('crud::fields.inc.wrapper_end')
--}}
