<table class="table table-striped mb-0">
    <tbody>
    @foreach ($crud->columns() as $column)
        <tr>
            <td>
                <strong>{!! $column['label'] !!}:</strong>
            </td>
            <td>
                @if (!isset($column['type']))
                    @include('crud::columns.text')
                @else
                    @if(view()->exists('vendor.backpack.crud.columns.'.$column['type']))
                        @include('vendor.backpack.crud.columns.'.$column['type'])
                    @else
                        @if(view()->exists('crud::columns.'.$column['type']))
                            @include('crud::columns.'.$column['type'])
                        @else
                            @include('crud::columns.text')
                        @endif
                    @endif
                @endif
            </td>
        </tr>
    @endforeach
    @if ($crud->buttons()->where('stack', 'line')->count())
        <tr>
            <td><strong>{{ trans('backpack::crud.actions') }}</strong></td>
            <td>
                @include('crud::inc.button_stack', ['stack' => 'line'])
            </td>
        </tr>
    @endif
    </tbody>
</table>
