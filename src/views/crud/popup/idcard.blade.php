@php
    if($avatar['column'] ?? null) {
        $value = data_get($entry, $avatar['column']);

        if($value) {
           $avatar['height'] = $avatar['height'] ?? "25px";
            $avatar['width'] = $avatar['width'] ?? "auto";
            $avatar['radius'] = $avatar['radius'] ?? "3px";
            $avatar['prefix'] = $avatar['prefix'] ?? '';

            if (preg_match('/^data\:image\//', $value)) { // base64_image
                $href = $src = $value;
            } elseif (isset($avatar['disk'])) { // image from a different disk (like s3 bucket)
                $href = $src = \Illuminate\Support\Facades\Storage::disk($avatar['disk'])->url($avatar['prefix'].$value);
            } else { // plain-old image, from a local disk
                $href = $src = asset( $avatar['prefix'] . $value);
            }
        }
    }
@endphp
<div class="modal-body p-0 bg-fh-grey">
    <div class="row">
        <div class="col-md-4 pr-0">
            @if( empty($value) )
                <i class="la la-user" style="font-size: 15em"></i>
            @else
                <img src="{{ $src }}" style="
                    max-height: {{ $avatar['height'] }};
                    width: {{ $avatar['width'] }};
                    border-radius: {{ $avatar['radius'] }};"
                />
            @endif
        </div>
        <div class="col-md-8 pl-0 align-middle m-auto">
            <div class="card no-padding no-border m-0">
                @include('crud::popup.columns')
            </div><!-- /.box-body -->
        </div>
    </div>
</div>
