        @if($menuitem['menu']['group'] ?? null)
            <li class="nav-item nav-dropdown"><a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-lg {{ $menuitem['menu']['group']['icon'] ?? $menuitem['icon'] ?? 'la-cog' }}"></i> {{ $menuitem['menu']['group']['label'] }}</a>
                <ul class="nav-dropdown-items">
                    @endif
                    @foreach($menuitem['menu']['menuitems'] as $subitem)
                        <li class="nav-item"><a class="nav-link" href="{{ \Baseline\Backpack\Helpers\RouteHelper::getRouteFromOperation(backpack_url($menuitem['basename']),$subitem['operation']) }}"><i class="nav-icon la la-lg {{ $subitem['icon'] ?? $menuitem['group']['icon'] ?? $menuitem['icon'] ?? 'la-cog' }}"></i> {{ $subitem['label'] }}</a></li>
                    @endforeach
                    @if($menuitem['menu']['group'] ?? null)
                </ul>
            </li>
        @endif
