<!-- Baseline Base CSS -->
<link rel="stylesheet" href="{{ asset('baseline/baseline.css?v='.config('unity.unity_version')) }}">

@if(isset($crud) && isset($crud->stylelets) && $crud->stylelets)
    <style>
        @foreach ($crud->stylelets as $stylelet)
            {!! $stylelet !!}
        @endforeach
    </style>
@endif

@if(isset($crud))
    @foreach ($crud->stylesheets as $stylesheet)
        <link rel="stylesheet" href="{{ asset($stylesheet) }}">
    @endforeach
@endif