@if(isset($crud))
    <div id="modal_holder">
        @foreach($crud->modals as $modal)
            {{ $modal }}
        @endforeach
    </div>
@endif
