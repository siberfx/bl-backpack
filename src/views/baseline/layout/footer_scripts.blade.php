<!-- Include Baseline Scripts -->
@if(isset($crud))
    @foreach($crud->modules as $module)
        <script type="module" src="{!! asset($module).(\Illuminate\Support\Str::startsWith($module,'http') ? "" : (preg_match('/\\?/',$module) ? '&' : '?')."v=".config('unity.unity_version')) !!}"></script>
    @endforeach
    @foreach($crud->modulelets as $modulelet)
        <script type="module">{!! $modulelet !!}</script>
    @endforeach
    @foreach($crud->scripts as $script)
        <script src="{!! asset($script).(\Illuminate\Support\Str::startsWith($script,'http') ? "" : (preg_match('/\\?/',$script) ? '&' : '?')."v=".config('unity.unity_version')) !!}"></script>
    @endforeach
    @foreach($crud->scriptlets as $scriptlet)
        <script>{!! $scriptlet !!}</script>
    @endforeach
@endif
<!-- Finish Baseline Scripts -->
