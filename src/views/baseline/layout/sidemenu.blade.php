@php($menu = Baseline\Backpack\Helpers\ControllerHelper::getMenuConfig())
@foreach($menu['orphans']['menuitems'] as $menuitem)
    @include('baseline.layout.sidemenuitem')
@endforeach
@foreach($menu['sections'] as $section)
    <li class="nav-title">{{ $section['label'] }}</li>
    @foreach($section['menuitems'] as $menuitem)
        @include('baseline.layout.sidemenuitem')
    @endforeach
@endforeach
