function togglePasswordChange(field_id, enable) {
    $("#"+field_id).prop('disabled',!enable);
    $("#"+field_id).val('');
    $("#"+field_id+"_confirmation").prop('disabled',!enable);
    $("#"+field_id+"_confirmation").val('');
}

$(document).ready(function() {
    @foreach($fields as $field)
        $("#change_{{ $field }}").click(function() {
            togglePasswordChange("{{ $field }}",$(this).prop('checked'));
        });
        togglePasswordChange("{{ $field }}",$("#change_{{ $field }}").attr('type') == 'checkbox' ? $("#change_{{ $field }}").prop('checked') : $("#change_{{ $field }}").val() == 1);
    @endforeach
});
