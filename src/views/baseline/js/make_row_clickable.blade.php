
let row = $(this).find('tr').each(function() {
    let id = $(this).find('td span[record-id]').attr('record-id');
    let cells = $(this).find('td');
    for(let i = 0; i < cells.length - 1; i++) {
        $(cells[i]).on('click',function() {
            location.href = '{!! $pre_id_url ?? '' !!}/' + id + '{!! $post_id_url ?? '' !!}';
        });
        $(cells[i]).css('cursor', 'pointer');
    }
});
