target_field = $("[{!! \Illuminate\Support\Str::contains($name,'=') ? $name : "name='$name'" !!}]");
@if(array_key_exists('disabled',$action))
    // special case of checkbox combo
    if(target_field.next().attr('type') == 'checkbox') {
        // do the checkbox too
        target_field.next().prop('disabled', {{ $action['disabled'] ? 'true' : 'false' }});
    }
    target_field.prop('disabled', {{ $action['disabled'] ? 'true' : 'false' }});
@endif

@if(array_key_exists('value',$action))
    // don't erase startup values during init
    if(target_field.val() == "" || !init_field_change) {
        target_field.val("{{ $action['value'] }}");
    }
@endif

@if(array_key_exists('checked',$action))
    if(!init_field_change) {
        // change the attached hidden value
        target_field.val({{ $action['checked'] ? 1 : 0 }});
        // tick the actual checkbox (next field)
        target_field.next().prop('checked',{{ $action['checked'] }});
    }
@endif

@if(array_key_exists('parent_visible',$action))
    target_field.parent().css('display','{{ $action['parent_visible'] ? '' : 'none' }}');
    @if($action['parent_visible'])
        //console.log("Triggering change to {{ $name }} because it has become visible");
        @if(!($action['notrigger'] ?? null))
            target_field.trigger("change");
            target_field.trigger("click");
        @endif
    @else
        //console.log("{{ $name }} has become invisible");
    @endif
@endif

@if(array_key_exists('grand_parent_visible',$action))
    target_field.parent().parent().css('display','{{ $action['grand_parent_visible'] ? '' : 'none' }}');
    @if($action['grand_parent_visible'])
        //console.log("Triggering change to {{ $name }} because it has become visible");
        target_field.trigger("change");
        target_field.trigger("click");
    @else
        //console.log("{{ $name }} has become invisible");
    @endif
@endif

@if(array_key_exists('parent2_visible',$action))
    target_field.parent().parent().css('display','{{ $action['parent2_visible'] ? '' : 'none' }}');
@endif

{{-- Execute custom code --}}
@if(array_key_exists('function',$action))
    {!! $action['function'] !!}
@endif
