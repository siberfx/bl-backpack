
$('button[type=submit]').on('click',function() { window.swal({
                title: "{{ $title ?? "Please wait..." }}",
                text: "{{ $text ?? "We are saving your data" }}",
                {!! $icon ?? null ? "icon: \"$icon\"," : "" !!}
                buttons: false,
                closeOnClickOutside: false
            }); });
