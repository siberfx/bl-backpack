@php($unset = $fields['_unset'] ?? null)
$(document).ready(function() {
    init_field_change = true;
    @foreach($fields as $field => $changes)
        @if($field == '_unset')
            @continue;
        @endif
        @if (preg_match('/^\w+$/',$field))
            field = $("[name={{$field}}]");
        @else
            field = $("{{$field}}");
        @endif
        if(field.is('select')) {
            field.change(function() {
                @if($unset)
                    @foreach($unset as $name => $action)
                        @include('baseline.js.field_change', ['action' => $action, 'name' => $name])
                    @endforeach
                @endif
                @php($default = null)
                @foreach($changes as $val => $actions)
                    @if($val == '*')
                        @php($default = $actions)
                    @else
                        if($(this).val() == "{{ $val }}") {
                            @foreach($actions as $name => $action)
                                @include('baseline.js.field_change', ['action' => $action, 'name' => $name])
                            @endforeach
                            return;
                        }
                        @endif
                    @endforeach
                    @if($default)
                        @foreach($default as $name => $action)
                            @include('baseline.js.field_change', ['action' => $action, 'name' => $name])
                        @endforeach
                    @endif
                });
            field.trigger("change");
        } else {
            if(field.is('input')) {
                if(field.attr('type') == 'hidden') {
                    // checkboxes aren't named anymore, so we look for the hidden field, check if the checkbox follows it (linked), then work with that
                    checkbox_field = field.next();
                    radios = field.parent().find('input[type=radio]');
                    if(checkbox_field.attr('type') == 'checkbox') {
                        field = checkbox_field;
                        field.click(function() {
                            @foreach($changes as $checked => $actions)
                                @if($unset)
                                    @foreach($unset as $name => $action)
                                        @include('baseline.js.field_change', ['action' => $action, 'name' => $name])
                                    @endforeach
                                @endif
                                @if($checked == 'checked')
                                    if($(this).prop('checked')) {
                                        @foreach($actions as $name => $action)
                                            @include('baseline.js.field_change', ['action' => $action, 'name' => $name])
                                        @endforeach
                                        return;
                                    }
                                @else
                                    if(!$(this).prop('checked')) {
                                        @foreach($actions as $name => $action)
                                            @include('baseline.js.field_change', ['action' => $action, 'name' => $name])
                                        @endforeach
                                    }
                                @endif
                            @endforeach
                        });
                        field.trigger("click");field.trigger("click");
                    } else if(radios.length > 0) {
                        radios.each(function() {
                            $(this).on('change',function() {
                                @php($default = null)
                                @foreach($changes as $val => $actions)
                                    @if($val == '*')
                                        @php($default = $actions)
                                    @else
                                        if(field.val() == "{{ $val }}") {
                                            @foreach($actions as $name => $action)
                                                @include('baseline.js.field_change', ['action' => $action, 'name' => $name])
                                            @endforeach
                                            return;
                                        }
                                    @endif
                                @endforeach
                                @if($default)
                                    @foreach($default as $name => $action)
                                        @include('baseline.js.field_change', ['action' => $action, 'name' => $name])
                                    @endforeach
                                @endif
                            });
                        });
                        radios.first().trigger('change');
                    }
                } else {
                    // We just do the work (nothing will change, so no trigger necessary)
                    @if($unset)
                        @foreach($unset as $name => $action)
                            @include('baseline.js.field_change', ['action' => $action, 'name' => $name])
                        @endforeach
                    @endif
                    @php($default = null)
                    @foreach($changes as $val => $actions)
                        @if($val == '*')
                            @php($default = $actions)
                        @else
                            if(field.val() == "{{ $val }}") {
                                @foreach($actions as $name => $action)
                                    @include('baseline.js.field_change', ['action' => $action, 'name' => $name])
                                @endforeach
                                return;
                            }
                        @endif
                    @endforeach
                    @if($default)
                        @foreach($default as $name => $action)
                            @include('baseline.js.field_change', ['action' => $action, 'name' => $name])
                        @endforeach
                    @endif
                }
            }
        }
    @endforeach
    init_field_change = false;
});
