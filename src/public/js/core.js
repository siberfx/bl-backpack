/**
 * Function to work through all the fields with the data copy class and add the even handlers
 */
function initDataCopiableFields() {
    $('.data-copy').each(function () {
        $(this).click(function (e) {
            e.stopPropagation();
            copyText($(this).attr('data-copy'));
        });
    });
}

/**
 * The actual copy command
 * @param text
 */
function copyText(text) {
    el = document.createElement('textarea');
    el.value = text;
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
}

function initTooltips() {
    $("[data-toggle=tooltip]").tooltip();
}


function moveModalsToHolder(modal_type) {
    let holder_id = 'modal_holder-' + modal_type;
    if ($('#' + holder_id).length === 0) {
        $('#modal_holder').prepend('<div id="' + holder_id + '"></div>');
    } else {
        $('#' + holder_id).empty();
    }
    // move them all into the holder
    $('div[id^="modal-' + modal_type +'"]').each(function () {
        $(this).appendTo($('#' + holder_id));
    });
}


$(function () {
    // Load the tooltips
    initTooltips();
    // Init Copiable fields
    initDataCopiableFields();
    // Now trigger them on the list draw
    $('#crudTable').on('draw.dt', function () {
        initDataCopiableFields();
        initTooltips();
    });
});
