<?php

return [
    'validator_valid_basename' => 'The basename must contain only lowercase alpha-numeric characters and/or dashes',
    'validator_valid_long' => 'Please enter a valid longitude',
    'validator_valid_lat' => 'Please enter a valid latitude',
    'validator_value_if' => 'The value of :attribute must be :value_if if :other_field is :other_value',
    'validator_in_tenant' => 'This :attribute is invalid',
];
