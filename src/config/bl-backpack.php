<?php

return [
    'google_api_key' => env('GOOGLE_API_KEY'),
    // schema folders
    'schema_folders' => [
        'default' => 'app/Schema/Admin',
        // add additional prefixes here
        //'api.enduser' => 'app/Schema/EndUser',
    ],
    'validation' => [
        'numbers_supported' => ['ZA', 'US', 'GB'],
    ],
    'menu' => [
        'sections' => [
            [
                'name' => 'main',
                'label' => 'Main'
            ],
            [
                'name' => 'tenant',
                'label' => 'System'
            ]
        ]
    ]
];
