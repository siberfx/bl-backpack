<?php

return [
    // this is the class which determines the partner
    'model' => 'App\Models\Tenant',
    // the name of the relation
    'relation' => 'tenant',
    // this is the foreign key in every class which refers back to the tenant (partner_id,tenant_id, etc etc)
    'foreign_key' => 'tenant_id',
    // we allow one tenant to be a super tenant
    'super_tenant' => 0,
    // what is the path to the super tenant area
    'global_baseroute' => 'global',
];
