<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Baseline\Backpack\Model\Traits\ProtectAttribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Tenant extends Model
{
    use CrudTrait, Notifiable, ProtectAttribute;

    protected $table = 'tenant';
    protected $primaryKey = 'id';
    public $timestamps = true;
    protected $hidden = [ "id" ];
    protected $fillable = [ "basename", "name", "enabled" ];

    protected $noupdate = [ 'basename' ];

    protected static function boot()
    {
        parent::boot();
    }

    public function users() {
        return $this->hasMany('App\Models\User');
    }

}
