<?php
/**
 * Created by PhpStorm.
 * User: darren
 * Date: 28/04/2020
 * Time: 00:02
 */

namespace App\Http\Controllers\Admin;


use App\Http\Requests\Admin\UserRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Baseline\Backpack\Controller\BaselineCoreController;
use Baseline\Backpack\Helpers\FilterHelper;
use Baseline\Backpack\Helpers\SecurityHelper;
use Baseline\Backpack\Operations\CreateOperation;
use Baseline\Backpack\Operations\DashboardOperation;
use Baseline\Backpack\Operations\DeleteOperation;
use Baseline\Backpack\Operations\ListOperation;
use Baseline\Backpack\Operations\SecretOperation;
use Baseline\Backpack\Operations\ToggleOperation;
use Baseline\Backpack\Operations\UpdateOperation;
use Illuminate\Support\Facades\Log;

class UserController extends CrudController
{
    use BaselineCoreController;

    // For list/index (add { index as baselineIndex; } if required to create index)
    use ListOperation;
    // for edit/update (add { update as baselineUpdate; } if required to create update)
    use UpdateOperation;
    // for create/store (add { store as baselineStore; } if required to create store)
    use CreateOperation { store as baselineStore; }
    // for the toggle function (add { toggle as baselineToggle; } if required to create toggle)
    use ToggleOperation;

    public static $crud_config = [
        'model' => 'App\Models\User',
        'entity_name_strings' => ['user', 'users'],
        'basename' => 'user',
        'menu' => [
            'group' => [
                'label' => 'Users',
                'icon' => 'la la-lg la-users-cog'
            ],
            'section' => 'tenant',
            'priority' => '10',
            'menuitems' => [
                [
                    'operation' => 'list',
                    'label' => 'List Users',
                    'icon' => 'la la-list'
                ],
                [
                    'operation' => 'create',
                    'label' => 'Add User',
                    'icon' => 'la la-plus'
                ]
            ],
        ]
    ];

    public function setup()
    {
        // Sets up the controller
        $this->setupController();
        // Any other setup thats not operation specific
    }

    protected function setupListOperation()
    {
        // Columns
        $this->crud->addColumn(['name' => 'name', 'label' => 'Name', 'type' => 'text']);
        $this->crud->addColumn(['name' => 'email', 'label' => 'Email', 'type' => 'email']);
        $this->crud->addColumn(['name' => 'enabled', 'label' => 'Enabled', 'type' => 'active_checkbox']);
        // Filters
        FilterHelper::addEnabledFilter($this->crud,"Enabled","enabled");
        /*
        $this->crud->addFilter(['type' => 'simple', 'name' => 'enabled_clients', 'label' => 'Enabled Clients'],
            false,
            function () {
                $this->crud->addClause('where', 'enabled', 1);
            });
        */
        // Add line buttons

        // set default order
        if (!request()->has('order')) { $this->crud->orderBy('name'); }
    }

    /**
     * This method preps the create options, setup validation, add your fields and other functionality prior to create / store method
     */
    protected function setupCreateOperation()
    {
        // Setup validation
        $this->crud->setValidation(UserRequest::class);
        // Setup fields
        $this->setupCommonFields(); // this is for fields common to both update and create
        // Fields for create only
        $this->crud->addField(['name' => 'email', 'label' => 'Email', 'type' => 'email', 'hint' => 'The email address (used as a username)'])
            ->beforeField('name');
        $this->crud->addField(['name' => 'password', 'label' => 'Password', 'type' => 'confirmed_password',
            'hint' => 'Enter the password for the user',
            'encrypt' => true])->beforeField('enabled');
        //$this->addPresetField('password',SecurityHelper::random_str(12));
        // Setup dependency fields (optional)
        //$this->setDependencyFields($this->getDependencyFields());
        // Set the save action (optional)
        // This redirects to a dashboard
        $this->crud->replaceSaveActions([
            'name' => 'Save',
            'redirect' => function($crud, $request, $itemId) {
                // return to the list
                return $this->crud->route;
                // return to a dashboard
                //return $this->crud->route . "/" . $this->crud->entry->id . "/dashboard";
            }
        ]);
        // Setup other stuff for the create page
        //$this->crud->stylesheets[] = '*****/*****.css';
        //$this->crud->scriptlets[] = 'console.log("Hi there")';
        //$this->crud->scripts[] = '**/**.js';
    }

    /**
     * This method allows you to setup the validation, fields and other stuff for edit/update
     */
    protected function setupUpdateOperation()
    {
        // Setup validation
        $this->crud->setValidation(UserRequest::class);
        // Setup fields
        $this->setupCommonFields(); // this is for fields common to both update and create
        // Update specific fields
        // Setup dependency fields (optional)
        //$this->setDependencyFields($this->getDependencyFields());
        // Set the save action (optional)
        // This redirects to a dashboard
        $this->crud->replaceSaveActions([
            'name' => 'Save',
            'redirect' => function($crud, $request, $itemId) {
                // return to the list
                return $this->crud->route;
                // return to a dashboard
                //return $this->crud->route . "/" . $request->input('id') . "/dashboard";
            }
        ]);
        // Setup other stuff for the create page
        //$this->crud->stylesheets[] = '*****/*****.css';
        //$this->crud->scriptlets[] = 'console.log("Hi there")';
        //$this->crud->scripts[] = '**/**.js';
    }

    /**
     * This method allows you to setup common fields for update and create
     */
    private function setupCommonFields()
    {
        $this->crud->addField(['name' => 'name', 'label' => 'Name', 'hint' => 'The name of the user']);
        $this->crud->addField(['name' => 'enabled', 'label' => 'Enabled', 'type'=> 'checkbox', 'hint' => 'Is this user able to login?', 'default' => 1]);
    }

}
