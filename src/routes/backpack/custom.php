<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => [
        config('backpack.base.web_middleware', 'web'),
        config('backpack.base.middleware_key', 'admin'),
        'tenant', // adding this for multi tenancy
    ],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    \Baseline\Backpack\Helpers\ControllerHelper::loadControllerRoutes(config('backpack.base.route_prefix'));
});