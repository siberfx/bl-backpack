<?php


namespace Baseline\Backpack\Requests;


use Baseline\Backpack\Responses\ValidationErrorResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Log;

class ApiRequest extends \Illuminate\Foundation\Http\FormRequest
{

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json(new ValidationErrorResponse($validator), 400));
    }
}
