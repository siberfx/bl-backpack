<?php
/**
 * Created by PhpStorm.
 * User: darren
 * Date: 2016/08/19
 * Time: 10:50 AM
 */

namespace Baseline\Backpack\Crud;


use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;
use Baseline\Backpack\Controller\CrudSecurity;
use Baseline\Backpack\Crud\Traits\Buttons;
use Baseline\Backpack\Crud\Traits\Dashboard;
use Baseline\Backpack\Crud\Traits\Popup;

class BaselineCrudPanel extends CrudPanel
{
    use CrudSecurity;
    use Popup;
    use Dashboard;
    use Buttons;

    public $controller;
    public $stylesheets = [];
    public $stylelets = [];
    public $scripts = [];
    public $modules = [];
    public $scriptlets = [];
    public $modulelets = [];
    public $modals = [];
    public $breadcrumbs = [];
    public $subtitle;
    public $title;
    public $table_loading = "";
    public $data = [];
    public $basename = "";
    public $baseroute = "";

    public function getEntries()
    {
        $this->limitSearchAndEntries($this->controller);
        return parent::getEntries();
    }


}
