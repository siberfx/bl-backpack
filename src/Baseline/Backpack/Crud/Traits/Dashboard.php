<?php

namespace Baseline\Backpack\Crud\Traits;

trait Dashboard
{
    /**
     * Gets the popup template.
     *
     * @return string name of the template file
     */
    public function getDashboardView()
    {
        return $this->get('dashboard.view') ?? 'crud::dashboard';
    }

    public function setDashboardView($view)
    {
        $this->set('dashboard.view',$view);
    }
}
