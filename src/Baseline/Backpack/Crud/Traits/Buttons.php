<?php


namespace Baseline\Backpack\Crud\Traits;


use Baseline\Backpack\Helpers\ControllerHelper;
use Baseline\Backpack\Helpers\MiscHelper;

trait Buttons
{

    function setButtonsIconsOnly(bool $iconsOnly)
    {
        $this->setOperationSetting('buttons.iconsOnly', $iconsOnly);
    }

    function showChildrenButtons(bool $show)
    {
        $this->setOperationSetting('children.buttons.show', $show);
    }

    function getChildrenButtons()
    {
        if ($this->getOperationSetting('children.buttons.show')) {
            $inline = [];
            $dropdown = [];
            $children = collect(ControllerHelper::getControllerConfigs())->where('parent', $this->basename)->toArray();
            foreach ($children as $child) {
                if (!MiscHelper::getNestedArrayValuesSafely($child, ['parent_index', 'disabled'])) {
                    if (MiscHelper::getNestedArrayValuesSafely($child, ['parent_index', 'type']) == 'inline') {
                        $inline[] = $this->buildButton($child);
                    } else {
                        $dropdown[] = $this->buildButton($child);
                    }
                }
            }
            uasort($inline, function ($a, $b) {
                return $a['priority'] <=> $b['priority'];
            });
            uasort($dropdown, function ($a, $b) {
                return $a['priority'] <=> $b['priority'];
            });
            return ['dropdown' => $dropdown, 'inline' => $inline];
        }
    }

    /**
     * @param $child
     * @return array
     */
    private function buildButton($child): array
    {
        return [
            'icon' => MiscHelper::getNestedArrayValuesSafely($child,['icon']) ?? 'la-cog',
            'basename' => MiscHelper::getNestedArrayValuesSafely($child,['basename']),
            'type' => MiscHelper::getNestedArrayValuesSafely($child, ['parent_index', 'type']) ?? 'inline',
            'label' => MiscHelper::getNestedArrayValuesSafely($child, ['parent_index', 'label']) ?? MiscHelper::ucfirst_all($child['entity_name_strings'][1]),
            'priority' => MiscHelper::getNestedArrayValuesSafely($child, ['parent_index', 'priority']) ?? 10,
        ];
    }
}
