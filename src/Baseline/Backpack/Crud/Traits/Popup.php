<?php

namespace Baseline\Backpack\Crud\Traits;

trait Popup
{
    /**
     * Gets the popup template.
     *
     * @return string name of the template file
     */
    public function getPopupView()
    {
        return $this->get('popup.view') ?? 'crud::popup';
    }

    public function setPopupView($view,$large = false)
    {
        $this->set('popup.view',$view);
        $this->setOperationSetting('modal_class','modal-info'.($large ? ' modal-lg' : ''),'popup');
    }
}
