<?php

namespace Baseline\Backpack\Commands;

use App\Models\Tenant;
use App\Models\User;
use Baseline\Backpack\Build\CreateTool;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

class CreateTransformer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bl-backpack:create_transformer {model} {outbound_fields_comma_delimited} {--namespace= : defaults to App\\\\Http\\\\Transformers} {--name= : class name, defaults to <model>Transformer}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a transformer for a model e.g. ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $model = $this->argument("model");
        $fields = $this->argument("outbound_fields_comma_delimited") ? explode(",", $this->argument("outbound_fields_comma_delimited")) : [];
        $name = $this->option('name');
        $namespace = $this->option('namespace');
        if ($namespace && !str_contains($namespace,'\\')) {
            throw new \Exception("No backslashes found in namespace, please ensure you escape your namespace with double backslashes (\\\\)");
        }

        $filename = CreateTool::createTransformer(
            $model,
            $fields,
            $name,
            $namespace
        );

        print("Created transformer " . $filename . "\n");

    }
}
