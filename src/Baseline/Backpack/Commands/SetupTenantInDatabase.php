<?php

namespace Baseline\Backpack\Commands;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;
use Illuminate\Console\Command;

class SetupTenantInDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bl-backpack:setup_tenant_in_database';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Setup the tenant table in the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $src = __DIR__ . "/../../../migrations";
        $target = base_path('/database/migrations');
        foreach ([
                     "2020_08_26_000000_create_tenant_table.php",
                     "2020_08_26_000001_add_tenant_fields_to_user.php",
                     "2020_08_26_085530_add_enabled_to_user.php"
                 ] as $file) {
            copy($src . "/" . $file, $target . '/' . $file);
        }
        print("Created migrations, running migrate\n");
        // now run artisan
        Artisan::call("migrate",['--force' => true]);
    }
}
