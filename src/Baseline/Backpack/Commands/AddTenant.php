<?php

namespace Baseline\Backpack\Commands;

use App\Models\Tenant;
use Illuminate\Console\Command;

class AddTenant extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tenant:add {name} {basename}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add a tenant';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $tenant_name = $this->argument('name');
        $tenant_basename = $this->argument('basename');
        if(preg_match('/^[a-z0-9\\-a-z0-9]+$/',$tenant_basename) && !Tenant::where('basename',$tenant_basename)->first()) {
            $tenant = new Tenant();
            $tenant->name = $tenant_name;
            $tenant->basename = $tenant_basename;
            $tenant->save();
        } else {
            throw new \Exception("Failed to add tenant $tenant_basename is invalid");
        }
    }
}
