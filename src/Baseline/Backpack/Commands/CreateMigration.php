<?php

namespace Baseline\Backpack\Commands;

use Baseline\Backpack\Build\CreateTool;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

class CreateMigration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bl-backpack:create_migration {table} {ancestors_comma_delimited} {fields_comma_delimited}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a migration (table create) for a table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $table = $this->argument("table");
        $fields = $this->argument("fields_comma_delimited") ? explode(",",$this->argument("fields_comma_delimited")) : [];
        $ancestors = $this->argument("ancestors_comma_delimited") ? explode(",",$this->argument("ancestors_comma_delimited")) : [];
        $filename = CreateTool::createMigration($table,$fields,$ancestors);
        print("Created migration ".$filename."\n");

    }
}
