<?php

namespace Baseline\Backpack\Commands;

use App\Models\Tenant;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class AddUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tenant:add_user {name} {email} {tenant_basename} {password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Adds a user to a tenant';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $email = $this->argument("email");
        $name = $this->argument("name");
        $tenant_basename = $this->argument("tenant_basename");
        $password = $this->argument('password');

        $tenant = Tenant::where('basename',$tenant_basename)->firstOrFail();
        if(User::where('email',$email)->first()) {
            throw new \Exception("User already exists");
        }
        $user = new User();
        $user->name = $name;
        $user->email = $email;
        $user->tenant_id = $tenant->id;
        $user->password = $password ? Hash::make($password) : "";
        $user->save();
    }
}
