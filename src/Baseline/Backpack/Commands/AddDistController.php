<?php

namespace Baseline\Backpack\Commands;

use Illuminate\Console\Command;

class AddDistController extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bl-backpack:add_controller {model}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add the predefined backpack controller for {model} (e.g. User)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $model = $this->argument('model');
        // Controller
        $prefix = __DIR__."/../../../app/Http/Controllers/Admin/";
        if(file_exists($prefix.$model."Controller.php")) {
            $dst_prefix = base_path('app/Http/Controllers/Admin/');
            if(!is_dir($dst_prefix)){
                mkdir($dst_prefix, 0755, true);
            }
            copy($prefix.$model."Controller.php", $dst_prefix.$model."Controller.php");
            print("Created ".$dst_prefix.$model."Controller.php\n");
        } else {
            print("No such controller ".$prefix.$model."Controller.php\n");
            die(255);
        }
        // Request
        $prefix = __DIR__."/../../../app/Http/Requests/Admin/";
        if(file_exists($prefix.$model."Request.php")) {
            $dst_prefix = base_path('app/Http/Requests/Admin/');
            if(!is_dir($dst_prefix)){
                mkdir($dst_prefix, 0755, true);
            }
            copy($prefix.$model."Request.php", $dst_prefix.$model."Request.php");
            print("Created ".$dst_prefix.$model."Request.php\n");
        }
        print("Remember to run composer dump-autoload\n");
    }
}
