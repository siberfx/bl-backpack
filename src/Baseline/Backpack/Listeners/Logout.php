<?php

namespace Baseline\Backpack\Listeners;

use Illuminate\Support\Facades\Log;

class Logout
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  IlluminateAuthEventsLogout  $event
     * @return void
     */
    public function handle(\Illuminate\Auth\Events\Logout $event)
    {
        Log::info("Logging out [".$event->user->email.", tenant=".session("tenant")."] and clearing session");
        session(['tenant' => null, config('tenant.foreign_key') => null]);
    }
}
