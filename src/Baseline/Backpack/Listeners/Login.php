<?php

namespace Baseline\Backpack\Listeners;

use Baseline\Backpack\Helpers\TenantHelper;
use Illuminate\Support\Facades\Log;

class Login
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  IlluminateAuthEventsLogin $event
     * @return void
     */
    public function handle(\Illuminate\Auth\Events\Login $event)
    {
        $tenant = TenantHelper::getTenantFromUser($event->user);
        if($tenant) {
            session(['tenant' => $tenant->basename, config('tenant.foreign_key') => $tenant->id]);
            Log::info("Logging in [".$event->user->email."] tenant=".$tenant->basename);
        } else {
            Log::info("Logging in [".$event->user->email."] no tenant assigned");
        }
    }
}
