<?php


namespace Baseline\Backpack;


use Baseline\Backpack\Crud\BaselineCrudPanel;
use Baseline\Backpack\Helpers\ValidationHelper;
use Illuminate\Console\Application as Artisan;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class BlBpServiceProvider extends ServiceProvider
{


    protected $commands = [
        \Baseline\Backpack\Commands\AddUser::class,
        \Baseline\Backpack\Commands\AddTenant::class,
        \Baseline\Backpack\Commands\SetupTenantInDatabase::class,
        \Baseline\Backpack\Commands\AddDistController::class,
        \Baseline\Backpack\Commands\AddDistModel::class,
        \Baseline\Backpack\Commands\CreateMigration::class,
        \Baseline\Backpack\Commands\CreateController::class,
        \Baseline\Backpack\Commands\CreateTransformer::class,
        \Baseline\Backpack\Commands\CreateModel::class,
        \Baseline\Backpack\Commands\CreateRequest::class,
        \Baseline\Backpack\Commands\CreateMvc::class,
    ];

    protected $tenant_middleware = [
        \Baseline\Backpack\Middleware\CheckUserAndTenantEnabled::class,
    ];

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot(Router $router)
    {
        $this->publish();

        // Make our crud panel the default
        $this->app->extend('crud', function () {
            return new BaselineCrudPanel();
        });

        // lets add this recursive method to collections
        \Illuminate\Support\Collection::macro('recursive', function () {
            return $this->map(function ($value) {
                if (is_array($value) || is_object($value)) {
                    return collect($value)->recursive();
                }

                return $value;
            });
        });

        // our built in validators
        $this->addValidators();

        $this->addBladeDirectives();
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(EventServiceProvider::class);
        $this->commands($this->commands);
        foreach ($this->tenant_middleware as $middleware_class) {
            $this->app->router->pushMiddlewareToGroup('tenant', $middleware_class);
        }
    }

    private function addValidators()
    {
        ValidationHelper::addValidator('valid_basename',"The :attribute be a valid basename");
        ValidationHelper::addValidator('valid_long',"The :attribute be a valid longitude");
        ValidationHelper::addValidator('valid_lat',"The :attribute be a valid latitude");
        ValidationHelper::addValidator('value_if',"The :attribute is required");
        ValidationHelper::addValidator('in_tenant', "The :attribute does not exist");
        ValidationHelper::addValidator('unique_in_parent', "The :attribute has already been taken");
        ValidationHelper::addValidator('in_parent', "The :attribute does not exist");
    }

    private function publish()
    {
        $crud_views = [
            // Crud Stuff
            __DIR__ . '/../../views/base/layouts' => resource_path('views/vendor/backpack/base/layouts'),
            __DIR__ . '/../../views/base/inc' => resource_path('views/vendor/backpack/base/inc'),
            __DIR__ . '/../../views/crud' => resource_path('views/vendor/backpack/crud'),
        ];

        $crud_lang = [
            __DIR__ . '/../../lang' => resource_path('lang'),
        ];

        $crud_assets = [
            // Baseline Stuff
            __DIR__ . '/../../public/css' => public_path('bl-backpack/css'),
            __DIR__ . '/../../public/js' => public_path('bl-backpack/js'),
            __DIR__ . '/../../views/baseline' => resource_path('views/baseline'),
        ];

        $crud_config = [
            __DIR__ . '/../../config' => config_path('/'),
            __DIR__ . '/../../routes/backpack' => base_path('/routes/backpack'),
        ];

        $tenant_migrations = [
            __DIR__ . '/../../migrations' => base_path('database/migrations'),
        ];

        $this->publishes($crud_views, 'views');
        $this->publishes($crud_assets, 'public');
        $this->publishes($crud_config, 'config');
        $this->publishes($crud_config, 'lang');
    }

    private function addBladeDirectives()
    {
        Blade::directive('include_once', function($expression) {
            $expression = Blade::stripParentheses($expression);
            return "<?php echo \\Baseline\\Backpack\\Helpers\\BladeDirectives::mustInclude($expression) ? \$__env->make({$expression}, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render() : ''; ?>";
        });
    }

}
