<?php


namespace Baseline\Backpack\Helpers;


use Illuminate\Support\Str;

class UIHelper
{

    /**
     * Method adds a javascript snippet to be called when a datatable is refreshed
     * function() { .... } is not necessary, just fill in the dots
     *
     * @param $crud
     * @param $javascript
     */
    public static function addJsOnListDraw($crud, $javascript)
    {
        $crud->scriptlets[] = "\$(function() { \$('#crudTable').on('draw.dt',function() { $javascript }); })";
    }


    public static function makeListRowClickToAddress($crud, $pre_id_url, $post_id_url = "")
    {
        if(!Str::endsWith($pre_id_url,'/')) { $pre_id_url = $pre_id_url."/"; }
        if($post_id_url && !Str::startsWith($post_id_url,'/')) { $post_id_url = "/".$post_id_url; }
        self::makeListRowClickable($crud,"function() { location.href = '$pre_id_url' + id + '$post_id_url'; }");
    }

    public static function makeListRowClickable($crud, $function)
    {
        self::addJsOnListDraw($crud, view('baseline.js.on_row_click', ['function' => $function]));
        $crud->addButtonFromView('line', 'row_click', 'row_click', 'end');
    }

    public static function addLoaderToSave($crud, $title = null, $text = null, $icon = null)
    {
        $crud->scriptlets[] = view('baseline.js.add_loader_to_save', ['title' => $title, 'text' => $text, 'icon' => $icon]);
    }
}
