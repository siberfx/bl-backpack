<?php
/**
 * Created by PhpStorm.
 * User: darren
 * Date: 02/06/2020
 * Time: 22:23
 */

namespace Baseline\Backpack\Helpers;


use Illuminate\Support\Facades\Log;

class FakeFieldHelper
{

    public static function getFakeColumnFromModel($model,$fake_column,$real_column_container) {
        $container = $model->$real_column_container;
        return $container
            && array_key_exists($fake_column,$container)
            ? $container[$fake_column] : null;
    }

}
