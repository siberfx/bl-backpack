<?php
/**
 * Created by PhpStorm.
 * User: darren
 * Date: 05/10/2019
 * Time: 13:33
 */

namespace Baseline\Backpack\Helpers;


use Baseline\Backpack\Helpers\ControllerHelper;
use Illuminate\Support\Facades\Route;

class RouteHelper
{

    static function is_create($field = null)
    {
        if(!$field) $field = ControllerHelper::getCurrentController()->basename;
        if (request()->method() == 'POST' && preg_match("/\\/$field(\\/|\$)/", request()->path()) && !Route::current()->hasParameter($field)) {
            return true;
        }
        return false;
    }

    static function is_update($field = null)
    {
        if(!$field) $field = ControllerHelper::getCurrentController()->basename;
//        if (request()->method() == 'POST' && preg_match("/\\/$field(\\/|\$)/", request()->path()) && Route::current()->hasParameter($field)) {
        if (in_array(request()->method(),['POST','PUT']) && preg_match("/\\/$field(\\/|\$)/", request()->path()) && Route::current()->hasParameter($field)) {
            return true;
        }
        return false;
    }

    static function parseRoute($route,$givenParameters = []) {
        $parameters = Route::current()->parameters();
        foreach($givenParameters as $parameter => $value) {
            $route = str_replace('{'.$parameter.'}',$value,$route);
        }
        foreach($parameters as $parameter => $value) {
            $route = str_replace('{'.$parameter.'}',$value,$route);
        }
        return $route;
    }

    static function getRouteFromOperation($base_route, $operation) {
        $result = $base_route;
        switch ($operation) {
            case 'create': $result = $base_route.'/create'; break;
            case 'update': $result = $base_route.'/update'; break;
        }
        return $result;
    }

    static function getParsedControllerRoute($basename, $additional_components = null) {
        return self::parseRoute(ControllerHelper::getControllerConfigByBasename($basename)['full_route'].($additional_components ? "/".$additional_components : ""));
    }

}
