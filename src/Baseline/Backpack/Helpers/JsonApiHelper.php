<?php


namespace Baseline\Backpack\Helpers;


use Neomerx\JsonApi\Schema\SchemaProvider;

class JsonApiHelper
{
    static function getRelationship($include, $includeRelationships, $related_obj)
    {
        return [
            $include => [
                SchemaProvider::SHOW_SELF => false,
                SchemaProvider::SHOW_RELATED => true,
                SchemaProvider::SHOW_DATA => isset($includeRelationships[$include]),
                SchemaProvider::DATA => function () use ($related_obj) {
                    return $related_obj;
                }
            ]];
    }
}
