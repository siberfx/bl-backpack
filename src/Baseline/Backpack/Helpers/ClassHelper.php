<?php
/**
 * Created by PhpStorm.
 * User: darren
 * Date: 09/05/2020
 * Time: 19:50
 */

namespace Baseline\Backpack\Helpers;


class ClassHelper
{
    public static function accessProtected($obj, $prop) {
        $reflection = new \ReflectionClass($obj);
        $property = $reflection->getProperty($prop);
        $property->setAccessible(true);
        return $property->getValue($obj);
    }

    public static function callIfExists($obj, $function_name, ...$arguments) {
        if(method_exists($obj,$function_name)) {
            return $obj->{$function_name}(...$arguments);
        }
        return null;
    }
}
