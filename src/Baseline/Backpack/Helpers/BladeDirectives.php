<?php
/**
 * Created by PhpStorm.
 * User: darren
 * Date: 24/04/2020
 * Time: 02:30
 */

namespace Baseline\Backpack\Helpers;

class BladeDirectives
{

    private static $included = [];
    public static function mustInclude($include) {
        if(!array_key_exists($include,self::$included)) {
            self::$included[$include] = 1;
            return true;
        }
        return false;
    }

}
