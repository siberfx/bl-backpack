<?php
/**
 * Created by PhpStorm.
 * User: darren
 * Date: 14/04/2018
 * Time: 16:30
 */

namespace Baseline\Backpack\Helpers;


use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;

class ValidationHelper
{

    public static function addValidator($name, $message = "")
    {
        if (method_exists(__CLASS__, $name)) {
            $trans_key = 'bl-backpack.validator_' . $name;
            $translation = trans($trans_key);
            if (($translation == $trans_key) && $message) {
                $translation = $message;
            }
            Validator::extend($name, '\Baseline\Backpack\Helpers\ValidationHelper::' . $name, $message);
        } else {
            throw new \Exception("There is no validation $name");
        }
    }

    public static function in_tenant($attribute, $value, $parameters) {
        // If the value is blank, don't check it.
        if(!$value) return true;
        // $paramaters[0] is the model
        return TenantHelper::checkCurrentTenantOwns($value,$parameters[0]);
    }

    public static function in_parent($attribute, $value, $parameters)
    {
        // in_parent:parent,attribute_collection,field_to_check
        if (count($parameters) < 3) {
            throw new \Exception('In parent must have at least 3 parameters : in_parent:parent,attribute_collection,field_to_check');
        }
        $parent_name = $parameters[0];
        $parent_child_collection_attribute = $parameters[1];
        $parent_child_attribute = $parameters[2];
        $config = ControllerHelper::getControllerConfigByBasename($parent_name);
        $parent_id = Route::current()->parameter($parent_name);
        if ($parent_id && $config) {
            $model = $config['model'];
            $parent = $model::findOrFail($parent_id);
            $query = $parent->$parent_child_collection_attribute()->where($parent_child_attribute, $value);
            return $query->first() != null;
        } else {
            throw new \Exception('Failed to find parent of type ' . $parent_name);
        }
    }

    public static function unique_in_parent($attribute, $value, $parameters) {
        // unique_in_parent:site,assets,ignore_id
        if(count($parameters) < 2) {
            throw new \Exception('Unique in parent must have at least 2 parameters : unique_in_parent:site,assets,ignore_id');
        }
        $parent_name = $parameters[0];
        $parent_collection_attribute = $parameters[1];
        $ignore_id = $parameters[2] ?? null;
        $config = ControllerHelper::getControllerConfigByBasename($parent_name);
        $parent_id = Route::current()->parameter($parent_name);
        if ($parent_id && $config) {
            $model = $config['model'];
            $parent = $model::findOrFail($parent_id);
            $query = $parent->$parent_collection_attribute()->where($attribute,$value);
            if($ignore_id) {
                $query->where('id','!=',$ignore_id);
            }
            return $query->first() == null;
        } else {
            throw new \Exception('Failed to find parent of type ' . $parent_name);
        }
    }

    public static function valid_basename($attribute, $value, $parameters)
    {
        return preg_match('/^[a-z0-9-]+$/', $value);
    }

    public static function valid_long($attribute, $value, $parameters)
    {
        return is_numeric($value) && ($value >= -180 && $value <= 180);
    }

    public static function valid_lat($attribute, $value, $parameters)
    {
        return is_numeric($value) && ($value >= -90 && $value <= 90);
    }

    public static function replacer_value_if($message, $attribute, $rule, $parameters)
    {
        return str_replace([':value_if', ':other_field', ':other_value'], $parameters, $message);
    }

    public static function value_if($attribute, $value, $parameters)
    {

        $must_value = $parameters[0];
        $other_field = $parameters[1];
        $other_value = $parameters[2];
        if (request()->input($other_field) == $other_value) {
            return $value == $must_value;
        }
        // return validation passed if the other value isn't what we need it to be
        return true;
    }
}
