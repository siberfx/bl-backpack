<?php
/**
 * Created by PhpStorm.
 * User: darren
 * Date: 20/08/2019
 * Time: 22:05
 */

namespace Baseline\Backpack\Helpers;


use Illuminate\Support\Facades\Log;

class TenantHelper
{

    public static function currentTenant() {
        $tenant_class = config('tenant.model');
        return $tenant_class::find(self::currentTenantId());
    }

    public static function currentTenantId() {
        return session(config('tenant.foreign_key'));
    }

    public static function getTenantFromUser($user) {
        $tenant_class = config('tenant.model');
        return $tenant_class::find($user->{config('tenant.foreign_key')});
    }

    public static function userAccessToCurrentTenant($user) {
        return $user->{config('tenant.foreign_key')} == self::currentTenantId();
    }

    public static function isValidTenant($id) {
        $tenant_class = config('tenant.model');
        return $tenant_class::where('id',$id)->where('enabled',1)->first() != null;
    }

    public static function checkCurrentTenantOwns($id,$class) {
        $current_tenant_id = self::currentTenant()->id;
        $obj = $class::find($id);
        if($obj) {
            $result = $obj->{config('tenant.foreign_key')} == $current_tenant_id;
            if(!$result) {
                LogHelper::hack("Possible hack attempt, trying to access a $class with id $id to tenant ".$current_tenant_id." when its tenant is ".$obj->{config('tenant.foreign_key')});
            }
            return $result;
        } else {
            Log::error("Failed to find an object of type $class with id $id");
        }
        return false;
    }

    public static function amISuperTenant() {
        // we changed this because you have to be hard bound to the super tenant
        return backpack_user()->{config('tenant.foreign_key')} == config('tenant.super_tenant');
    }

    public static function ownedObjectsQuery($model,$tenant_id = null)
    {
        if(!$tenant_id) $tenant_id = self::currentTenantId();
        return $model::where(config('tenant.foreign_key'),$tenant_id);
    }

    public static function newOwnedObject($model,$tenant_id = null)
    {
        if(!$tenant_id) $tenant_id = self::currentTenantId();
        return new $model([
            config('tenant.foreign_key') => $tenant_id
        ]);
    }

    public static function validationRuleUniqueInTenant($table,$column,$ignore = 'NULL') {
        return "unique:$table,$column,$ignore,id,".config('tenant.foreign_key').",".self::currentTenantId();
    }
}
