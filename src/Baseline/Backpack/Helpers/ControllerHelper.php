<?php
/**
 * Created by PhpStorm.
 * User: darren
 * Date: 24/04/2020
 * Time: 23:19
 */

namespace Baseline\Backpack\Helpers;


use Baseline\Backpack\Helpers\ClassFinder;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

class ControllerHelper
{

    private static $controllerConfigs;
    private static $controllerConfigsByClass;
    private static $menu;

    /** This method builds the rootmap from the admin controllers
     *
     */
    public static function getControllerConfigs()
    {
        if (self::$controllerConfigs != null) {
            return self::$controllerConfigs;
        }
        // we have to build it
        $classfinder = new ClassFinder();
        $classes = $classfinder->getClassesByNamespace('App\Http\Controllers\Admin');
        $configs = [];
        foreach ($classes as $class) {
            if (isset($class::$crud_config) && !Str::endsWith($class,'TemplateCrudController') && (!(new \ReflectionClass($class))->isAbstract())) {
                $configs[$class::$crud_config['basename']] = array_merge($class::$crud_config,
                    ['controller' => [
                        'class' => $class,
                        'class_name' => preg_replace('/.*\\\\/','', $class)
                    ]
                    ]);
            }
        }
        $new_route_map = [];
        foreach($configs as $basename => $crud_config) {
            $routeObj = $crud_config;
            $routeObj['route'] = $basename;
            if(!isset($routeObj['baseroute'])) { $routeObj['baseroute'] = config('backpack.base.route_prefix'); }
            $parent = key_exists('parent', $crud_config) ? $crud_config['parent'] : null;
            if ($parent) {
                while ($parent) {
                    $parent_config = $configs[$parent];
                    $routeObj['route'] = $parent.'/{'.$parent.'}/'.$routeObj['route'];
                    $parent = key_exists('parent', $parent_config) ? $parent_config['parent'] : null;
                }
            }
            $routeObj['full_route'] = $routeObj['baseroute']."/".$routeObj['route'];
            $new_route_map[$basename] = $routeObj;
        }
        self::$controllerConfigs = $new_route_map;
        // Setup the by class lookup
        foreach($new_route_map as $basename => $config) {
            self::$controllerConfigsByClass[$config['controller']['class']] = $config;
        }
        //dd(self::$controllerConfigs);
        return self::$controllerConfigs;
    }

    private static $current_controller;
    static function getCurrentController() {
        return self::$current_controller;
    }

    static function setCurrentController($controller) {
        self::$current_controller = $controller;
    }

    static function loadControllerRoutes($baseroute)
    {
        foreach(self::getControllerConfigs() as $basename => $config) {
            if($config['baseroute'] == $baseroute) {
                Route::crud($basename, $config['controller']['class_name']);
            }
        }
    }

    public static function getMenuConfig() {
        if(!self::$menu) {
            $menu = collect([
                'orphans' => [],
                'sections' => config('bl-backpack.menu.sections'),
            ])->recursive();
            $menu->get('sections')->each(function($section) { $section->put('menuitems',collect([])); });
            $menu->get('orphans')->put('menuitems',collect([]));

            foreach (self::getControllerConfigs() as $basename => $config) {
                if($model_menu = $config['menu'] ?? null) {
                    if($model_menu['section'] ?? null) {
                        if ($section = $menu['sections']->where('name', $model_menu['section'])->first()) {
                            $section->get('menuitems')->push($config);
                        }
                    } else {
                        $menu->get('orphans')->get('menuitems')->push($config);
                    }
                }
            }
            // sort them by priority
            $menu->get('sections')->each(function($section) { $section->put('menuitems',$section->get('menuitems')->sortBy('menu.priority')); });
            $menu->get('orphans')->put('menuitems',$menu->get('orphans')->get('menuitems')->sortBy('menu.priority'));
            self::$menu = $menu->toArray();
        }
        return self::$menu;
    }

    public static function getControllerConfigByBasename($basename)
    {
        return self::$controllerConfigs[$basename] ?? null;
    }

    public static function getControllerConfig($obj)
    {
        return self::$controllerConfigsByClass['\\'.get_class($obj)];
    }

    public static function getParentIndexLabelFromControllerConfig($config)
    {
        if ($value = MiscHelper::getNestedArrayValuesSafely($config,['parent_index','label'])) {
            return $value;
        } else {
            return implode(" ", array_map('ucfirst', explode(" ", $config['entity_name_strings'][1])));
        }
    }
}
