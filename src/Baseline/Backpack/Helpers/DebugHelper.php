<?php


namespace Baseline\Backpack\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class DebugHelper
{

    public static function logSqlQueries() {
        DB::listen(function ($query) {
            Log::debug("DB[".getmypid()."] ".$query->sql . " : [" . implode(",", $query->bindings) . "] : " . $query->time);
        });

    }

}
