<?php
/**
 * Created by PhpStorm.
 * User: darren
 * Date: 2018/12/06
 * Time: 4:10 PM
 */

namespace Baseline\Backpack\Helpers;


use Illuminate\Support\Facades\Log;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;

class MiscHelper
{
    public static function getRequesterIp()
    {
        return request()->header('X-Forwarded-For', request()->ip());
    }

    static function ucfirst_all($string) {
        return implode(" ", array_map('ucfirst', explode(" ", $string)));
    }

    /**
     * Checks null at each level and retrieves final value or null
     *
     * @param $array
     * @param $keys
     */
    public static function getNestedArrayValuesSafely($array,$keys) {
        if($keys && count($keys)) {
            if(is_array($array) && isset($array[$keys[0]])) {
                return self::getNestedArrayValuesSafely($array[$keys[0]],array_slice($keys,1));
            } else {
                return null;
            }
        }
        return $array;
    }

    public static $numbersSupported = ['ZA', 'US', 'GB'];
    public static function formatNumber($number, $countries = 'default')
    {
        if ($countries == 'default') {
            $countries = self::$numbersSupported;
        }
        $util = PhoneNumberUtil::getInstance();
        $done = null;

        try {
            $parsed = $util->parse($number);
            if($util->isValidNumber($parsed)) {
                $done = $util->format($parsed, PhoneNumberFormat::INTERNATIONAL);
            } else {
                //Log::debug("Could not autodetect number $number");
            }
        } catch (\Exception $e) {
            //Log::debug("Could not autodetect number $number : " . $e->getMessage());
        }

        if (!$done) {
            //Log::debug("Will try parse for " . implode(",", $countries));
            foreach ($countries as $country) {
                try {
                    $parsed = $util->parse($number, $country);
                    if($util->isValidNumber($parsed)) {
                        $done = $util->format($util->parse($number, $country), PhoneNumberFormat::INTERNATIONAL);
                        //Log::debug("Parsed number to $done for $country");
                        break;
                    } else {
                        //Log::debug("Could not parse number $number for $country");
                    }
                } catch (\Exception $e) {
                    //Log::debug("Failed to format $number for $country : " . $e->getMessage());
                }
            }
        }

        return $done ?? $number;
    }

    public static function convertUptimeToSecs($uptime)
    {
        $parts = [];
        preg_match_all('/(?:([0-9]+)y|)(?:([0-9]+)w|)(?:([0-9]+)d|)(?:([0-9]+)h|)(?:([0-9]+)m|)(?:([0-9]+)s|)/', $uptime, $parts);
        $total =
            (intval($parts[1][0]) * 31536000) + // years
            (intval($parts[2][0]) * 604800) + // weeks
            (intval($parts[3][0]) * 86400) + // days
            (intval($parts[4][0]) * 3600) + // hours
            (intval($parts[5][0]) * 60) + // minutes
            intval($parts[6][0]); // seconds
        return $total;
    }

    public static function camelCaseToUnderscores($string) {
        return $output = strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $string));
    }

    public static function underscoresToCamelCase($string, $first_capital = false) {
        $output = preg_replace_callback('/_(.)/',function ($matches) { return strtoupper($matches[1]); },$string);
        if($first_capital) {
            $output = ucfirst($output);
        }
        return $output;
    }

    public static function underscoresToSpaceUcfirst($string, $first_capital = false) {
        $output = preg_replace_callback('/_(.)/',function ($matches) { return " ".strtoupper($matches[1]); },$string);
        if($first_capital) {
            $output = ucfirst($output);
        }
        return $output;
    }

}
