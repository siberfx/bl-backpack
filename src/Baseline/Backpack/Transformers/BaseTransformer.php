<?php


namespace Baseline\Backpack\Transformers;


class BaseTransformer extends \League\Fractal\TransformerAbstract
{

    public function smap($array, $obj, $include_null = true)
    {
        $results = [];
        foreach ($array as $key) {
            if (!$include_null && $obj->$key === null) {
                continue;
            }
            $results[$key] = $obj->$key;
        }
        return $results;
    }
}
