<?php

namespace Baseline\Backpack\Middleware;

use App\Providers\RouteServiceProvider;
use Baseline\Backpack\Helpers\TenantHelper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class CheckUserAndTenantEnabled
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        /*
        Log::error("TEST");
        $user = backpack_user();
        if ($user
            && $user->enabled
            && TenantHelper::isValidTenant(TenantHelper::currentTenantId())
        ) {
            Log::error("User and/or Tenant is not valid : " .($user ? $user->email : "unknown"));
            session(['tenant' => null, config('tenant.foreign_key') => null]);
            //return route("backpack.auth.logout");
        }
        */
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, \Closure $next, $guard = null)
    {
        $user = backpack_user();
        if ($user
            && $user->enabled
            && TenantHelper::isValidTenant(TenantHelper::currentTenantId())
        ) {
            return $next($request);
        }

        Log::error("User and/or Tenant is not valid [".TenantHelper::currentTenantId()."] : " .($user ? $user->email." [".($user->enabled ? "enabled" : "disabled")."]" : "unknown"));
        session(['tenant' => null, config('tenant.foreign_key') => null]);
        return redirect(route("backpack.auth.logout"));
    }
}
