<?php


namespace Baseline\Backpack\Middleware;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use JsonSchema\Constraints\Constraint;
use JsonSchema\Validator;

class CheckJsonSchema
{
    private $errors = [];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, \Closure $next, $guard = null)
    {
        $type = preg_replace('/[^\w_-]/','.',Route::getCurrentRoute()->getName());
        $body = $request->getContent();
        if(!$this->validateRequestReturnErrors($type,json_decode($body))) {
            return response()->json([ 'errors' => $this->errors ],400);
        }
        return $next($request);
    }

    /**
     * @param $type
     * @param $json
     */
    protected function validateRequestReturnErrors($type, $request)
    {
        // Check against schema
        $schema_folder = $this->get_folder($type);
        $schema_file = base_path($schema_folder."/$type.schema");
        if (!file_exists($schema_file)) {
            Log::error("Failed to process message of type $type, it has no schema in $schema_folder");
            $this->errors[] = ['message' => 'Server Error', 'code' => 'UNKNOWN_ERROR'];
            return false;
        }
        $schemaString = file_get_contents($schema_file);
        $schema = json_decode($schemaString);
//        Log::debug(print_r($schema,true));
        $validator = new Validator();
        $validator->validate($request, $schema, Constraint::CHECK_MODE_APPLY_DEFAULTS);
        if(!$validator->isValid()) {
            $this->errors = [];
            foreach ($validator->getErrors() as $error) {
                $myerror = collect($error)->only(['message','property','constraint'])->toArray();
                Log::warning("Problem validating $type api request ".json_encode($request)." : ".$error['pointer'] . " : ". $error['message']);
                $myerror = $this->sanitize_error($myerror);
                $this->errors[] = $myerror;
            }
            return false;
        }
        return true;
    }

    private function sanitize_error($error) {
        if($error['constraint'] == 'pattern') {
            $error['technical_message'] = $error['message'];
            $error['message'] = "Incorrect format";
        } else {
            $error['technical_message'] = $error['message'];
        }
        return $error;
    }

    private function get_folder($action) {
        $schema_folders = config('bl-backpack.schema_folders',['default' => 'app/Schema/Admin' ]);
        foreach($schema_folders as $prefix => $folder) {
            if(Str::startsWith($action,$prefix)) {
                //Log::debug("Found $prefix => $folder match for $action");
                return $folder;
            }
        }
        return $schema_folders['default'];
    }
}
