<?php

namespace Baseline\Backpack;

use Baseline\Backpack\Helpers\DebugHelper;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'Illuminate\Auth\Events\Login' => [ 'Baseline\Backpack\Listeners\Login@handle' ],
        'Illuminate\Auth\Events\Logout' => [ 'Baseline\Backpack\Listeners\Logout@handle' ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        // If we are debugging SQL, this will print the SQL to error log
        if(config('app.db_debug')) {
            DebugHelper::logSqlQueries();
        }

    }
}
