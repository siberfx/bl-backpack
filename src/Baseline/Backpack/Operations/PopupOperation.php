<?php

namespace Baseline\Backpack\Operations;

use Baseline\Backpack\Helpers\ClassHelper;
use Baseline\Backpack\Helpers\ControllerHelper;
use Baseline\Backpack\Helpers\UIHelper;
use Illuminate\Support\Facades\Route;

trait PopupOperation
{
    /**
     * Define which routes are needed for this operation.
     *
     * @param string $segment    Name of the current entity (singular). Used as first URL segment.
     * @param string $routeName  Prefix of the route name.
     * @param string $controller Name of the current CrudController.
     */
    protected function setupPopupRoutes($segment, $routeName, $controller)
    {
        $config = ControllerHelper::getControllerConfigByBasename($routeName);
        Route::get($config['route'].'/{'.$routeName.'}/popup', [
            'as'        => $routeName.'.popup',
            'uses'      => $controller.'@popup',
            'operation' => 'popup',
        ]);
    }

    /**
     * Add the default settings, buttons, etc that this operation needs.
     */
    protected function setupPopupDefaults()
    {
        $this->crud->allowAccess('popup');
        $this->crud->operation('popup', function () {
            $this->crud->loadDefaultOperationSettingsFromConfig();
        });

        $this->crud->operation('list', function () {
            UIHelper::addJsOnListDraw($this->crud,"moveModalsToHolder('popup')");
            UIHelper::makeListRowClickable($this->crud,'function() { tr.find("button[data-target=\'#modal-popup\']").click(); }' );
            $this->crud->addButton('line', 'popup', 'view', 'crud::buttons.popup', 'beginning');
        });
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function popup($id)
    {
        // Check access
        $this->checkAccessOrFail();
        $this->crud->hasAccessOrFail('popup');

        // get entry ID from Request (makes sure its the last ID for nested resources)
        $id = $this->crud->getCurrentEntryId() ?? $id;

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['crud'] = $this->crud;

        // cycle through columns
        foreach ($this->crud->columns() as $key => $column) {

            // remove any autoset relationship columns
            if (array_key_exists('model', $column) && array_key_exists('autoset', $column) && $column['autoset']) {
                $this->crud->removeColumn($column['key']);
            }

            // remove any autoset table columns
            if ($column['type'] == 'table' && array_key_exists('autoset', $column) && $column['autoset']) {
                $this->crud->removeColumn($column['key']);
            }

            // remove the row_number column, since it doesn't make sense in this context
            if ($column['type'] == 'row_number') {
                $this->crud->removeColumn($column['key']);
            }

            // remove the character limit on columns that take it into account
            if (in_array($column['type'], ['text', 'email', 'model_function', 'model_function_attribute', 'phone', 'row_number', 'select'])) {
                $this->crud->modifyColumn($column['key'], ['limit' => ($column['limit'] ?? 999)]);
            }
        }

        // remove preview button from stack:line
        $this->crud->removeButton('popup');

        // remove bulk actions colums
        $this->crud->removeColumns(['blank_first_column', 'bulk_actions']);

        ClassHelper::callIfExists($this, 'before_popup');
        try {
            $result = view($this->crud->getPopupView(), $this->data)->render();
        } catch (\Exception $e) {
            ClassHelper::callIfExists($this, 'after_popup', $this->getId(), null, false, $e);
            throw $e;
        }
        return ClassHelper::callIfExists($this, 'after_popup', $this->getId(), $result, true, null)
            ?? $result;

    }
}
