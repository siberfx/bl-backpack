<?php
/**
 * Created by PhpStorm.
 * User: darren
 * Date: 25/04/2020
 * Time: 23:49
 */

namespace Baseline\Backpack\Operations;


use Baseline\Backpack\Controller\CrudSecurity;
use Baseline\Backpack\Helpers\ClassHelper;
use Baseline\Backpack\Helpers\ControllerHelper;
use Backpack\CRUD\app\Library\CrudPanel\Traits\Search;
use Illuminate\Support\Facades\Route;

trait ListOperation
{
    // We need this for our automatic limits
    use CrudSecurity;
    // we want to use the functionality from backpack, but we need to stick our own stuff in there
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation {
        index as backpackIndex;
        search as backpackSearch;
        setupListDefaults as backpackSetupListDefaults;
    }

    /**
     * We need to create our own here because theirs doesn't cater for nested routes
     * @param $segment
     * @param $routeName
     * @param $controller
     */
    protected function setupListRoutes($segment, $routeName, $controller)
    {
        $config = ControllerHelper::getControllerConfigByBasename($routeName);
        Route::get($config['route'].'/', [
            'as'        => $routeName.'.index',
            'uses'      => $controller.'@index',
            'operation' => 'list',
        ]);

        Route::post($config['route'].'/search', [
            'as'        => $routeName.'.search',
            'uses'      => $controller.'@search',
            'operation' => 'list',
        ]);

        Route::get($config['route'].'/{'.$routeName.'}/details', [
            'as'        => $routeName.'.showDetailsRow',
            'uses'      => $controller.'@showDetailsRow',
            'operation' => 'list',
        ]);
    }

    public function setupListDefaults() {
        $this->backpackSetupListDefaults();
        $this->crud->showChildrenButtons(true);
    }

    public function index() {
        // Check that we have access to this route
        $this->checkAccessOrFail();
        // Setup breadcrumbs
        $this->data['breadcrumbs'] = $this->getBreadcrumbs();
        // Every search result, load the tooltips again
        $this->crud->scriptlets[] = '$(function() { $(\'#crudTable\').on( \'draw.dt\', function() { $("[data-toggle=tooltip]").tooltip(); }); });';
        // Every search result, init the copyable fields
        $this->crud->scriptlets[] = '$(function() { $(\'#crudTable\').on( \'draw.dt\', function() { initDataCopiableFields() }); });';
        // Call backpacks function()
        ClassHelper::callIfExists($this,'before_index');
        try {
            $result = $this->backpackIndex();
        } catch (\Exception $e) {
            ClassHelper::callIfExists($this,'after_index',null,false,$e);
            throw $e;
        }
        return ClassHelper::callIfExists($this,'after_index',$result,true,null)
            ?? $result;
    }

    /**
     * The search method
     */
    public function search() {
        // Checks if we have access to this route
        $this->checkAccessOrFail();
        // Adds additional parameters to the SQL to ensure partner and parent route parameter scope
        $this->limitSearchAndEntries($this);
        // Lets get our count before backpack applies the searches and filters
        $totalRows = $this->crud->count();
        // Adding the children buttons
        $this->crud->addButtonFromView('line', 'children', 'children', 'beginning');
        // Adding the record information holder
        $this->crud->addButtonFromView('line', 'record_info_holder', 'record_info_holder', 'end');
        // Call backpacks function()
        ClassHelper::callIfExists($this,'before_search');
        try {
            $value = $this->backpackSearch();
        } catch (\Exception $e) {
            ClassHelper::callIfExists($this,'after_search',null,false,$e);
            throw $e;
        }
        // Backpack does the 'total' count without any core where clause, meaning it returns the count of every record in the table
        // This is bad (lose partner and parent scope), so we need a 'total' with our limits
        $value['recordsTotal'] = $totalRows;
        return ClassHelper::callIfExists($this,'after_search',$value,$this->isSuccessful($value),null)
            ?? $value;
    }
}
