<?php
/**
 * Created by PhpStorm.
 * User: darren
 * Date: 26/04/2020
 * Time: 00:30
 */

namespace Baseline\Backpack\Operations;

use Baseline\Backpack\Controller\BaselineCoreController;
use Baseline\Backpack\Controller\CrudSecurity;
use Baseline\Backpack\Helpers\ClassHelper;
use Baseline\Backpack\Helpers\ControllerHelper;
use Baseline\Backpack\Helpers\LogHelper;
use Illuminate\Support\Facades\Route;

trait ShowOperation
{
    use CrudSecurity;
    use BaselineCoreController;

    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation {
        show as backpackShow;
    }

    /**
     * We need to override this method because it uses {id} as a parameter name, this doesn't work for nested parameters
     * Check this everytime for updated methods
     *
     * @param $segment
     * @param $routeName
     * @param $controller
     */
    protected function setupShowRoutes($segment, $routeName, $controller)
    {
        $config = ControllerHelper::getControllerConfigByBasename($routeName);
        Route::get($config['route'].'/{'.$routeName.'}/show', [
            'as'        => $routeName.'.show',
            'uses'      => $controller.'@show',
            'operation' => 'show',
        ]);

    }

    public function show($id) {
        // Check access
        $this->checkAccessOrFail();
        // Stop the ui from printing "Back to list"
        $this->crud->denyAccess('list');
        // Set the heading
        if(!$this->crud->getHeading()) $this->crud->setHeading($this->crud->getCurrentEntry()->name);
        // Set the subheading
        if(!$this->crud->getSubHeading()) $this->crud->setSubHeading($this->subheading ?? 'Edit');
        // Set the breadcrumbs
        $this->data['breadcrumbs'] = $this->getBreadcrumbs();
        // Run the backpack
        ClassHelper::callIfExists($this, 'before_show');
        try {
            $result = $this->backpackShow($id);
        } catch (\Exception $e) {
            ClassHelper::callIfExists($this, 'after_show', $this->getId(), null, false, $e);
            throw $e;
        }
        return ClassHelper::callIfExists($this, 'after_show', $this->getId(), $result, true, null)
            ?? $result;

    }
}
