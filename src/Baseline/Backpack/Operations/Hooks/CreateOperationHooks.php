<?php


namespace Baseline\Backpack\Operations\Hooks;


interface CreateOperationHooks
{
    function before_create();
    function after_create($result, bool $success, ?\Exception $exception);

    function before_store();
    function after_store($result, bool $success, ?\Exception $exception);
}
