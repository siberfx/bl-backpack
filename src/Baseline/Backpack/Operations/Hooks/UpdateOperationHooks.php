<?php


namespace Baseline\Backpack\Operations\Hooks;


interface UpdateOperationHooks
{
    function before_edit($id);
    function after_edit($id,$result, bool $success, ?\Exception $exception);

    function before_update($id);
    function after_update($id,$result, bool $success, ?\Exception $exception);
}
