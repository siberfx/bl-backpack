<?php


namespace Baseline\Backpack\Operations\Hooks;


interface ListOperationHooks
{
    function before_index();
    function after_index($result, bool $success, ?\Exception $exception);

    function before_search();
    function after_search($result, bool $success, ?\Exception $exception);
}
