<?php
/**
 * Created by PhpStorm.
 * User: darren
 * Date: 26/04/2020
 * Time: 00:30
 */

namespace Baseline\Backpack\Operations;

use Baseline\Backpack\Controller\CrudSecurity;
use Baseline\Backpack\Helpers\ClassHelper;
use Baseline\Backpack\Helpers\ControllerHelper;
use Illuminate\Support\Facades\Route;

trait DashboardOperation
{
    use CrudSecurity;
    private $dashboard_view;

    protected function setupDashboardRoutes($segment, $routeName, $controller)
    {
        $config = ControllerHelper::getControllerConfigByBasename($routeName);
        Route::get($config['route'] . '/{' . $routeName . '}/dashboard', [
            'as' => $routeName . '.dashboard',
            'uses' => $controller . '@dashboard',
            'operation' => 'dashboard',
        ]);
        Route::get($config['route'] . '/{' . $routeName . '}/show', [
            'as' => $routeName . '.redirect_to_dashboard',
            'uses' => $controller . '@redirect_to_dashboard',
            'operation' => 'redirect_to_dashboard',
        ]);
    }

    protected function setupDashboardDefaults()
    {
        $this->crud->allowAccess('dashboard');

        $this->crud->operation('list', function () {
            $this->crud->addButton('line', 'dashboard', 'view', 'crud::buttons.dashboard', 'beginning');
        });
    }

    public function redirect_to_dashboard()
    {
        return redirect(preg_replace('%/show$%', 'dashboard', request()->path()) . "/dashboard");
    }

    public function setDashboardView($view)
    {
        $this->dashboard_view = $view;
    }

    public function dashboard()
    {
        // Check access
        $this->checkAccessOrFail();
        // Set the heading
        if (!$this->crud->getHeading()) $this->crud->setHeading($this->crud->getCurrentEntry()->name);
        // Set the subheading
        if (!$this->crud->getSubHeading()) $this->crud->setSubHeading($this->subheading ?? 'Dashboard');
        // Set the breadcrumbs
        $this->data['breadcrumbs'] = $this->getBreadcrumbs();
        // Make sure entry gets into view
        $this->data['entry'] = $this->crud->getCurrentEntry();
        // Make sure crud
        $this->data['crud'] = $this->crud;
        // show the view
        ClassHelper::callIfExists($this, 'before_dashboard');
        try {
            $result = view($this->crud->getDashboardView(), $this->data)->render();
        } catch (\Exception $e) {
            ClassHelper::callIfExists($this, 'after_dashboard', $this->getId(), null, false, $e);
            throw $e;
        }
        return ClassHelper::callIfExists($this, 'after_dashboard', $this->getId(), $result, true, null)
            ?? $result;

    }

}
