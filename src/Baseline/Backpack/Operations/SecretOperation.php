<?php
/**
 * Created by PhpStorm.
 * User: darren
 * Date: 26/04/2020
 * Time: 00:30
 */

namespace Baseline\Backpack\Operations;


use Baseline\Backpack\Controller\CrudSecurity;
use Baseline\Backpack\Helpers\ClassHelper;
use Baseline\Backpack\Helpers\ControllerHelper;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

trait SecretOperation
{
    use CrudSecurity;

    private $secret_attribute = "secret";

    /**
     * @param mixed $secret_attribute
     */
    public function setSecretAttribute($secret_attribute)
    {
        $this->secret_attribute = $secret_attribute;
    }


    public function getSecretAttribute() {
        return $this->secret_attribute;
    }

    /**
     * @param $segment
     * @param $routeName
     * @param $controller
     */
    protected function setupSecretRoutes($segment, $routeName, $controller)
    {
        $config = ControllerHelper::getControllerConfigByBasename($routeName);
        Route::get($config['route'].'/{'.$routeName.'}/secret', [
            'as'        => $routeName.'.secret',
            'uses'      => $controller.'@secret',
            'operation' => 'secret',
        ]);
    }


    public function secret() {
        // Check access
        $this->checkAccessOrFail();
        // get current entry
        $object = $this->crud->getCurrentEntry();
        // get the secret
        ClassHelper::callIfExists($this,'before_secret',$this->getId());
        if($object) {
            $result = response()->json(['secret' => $object->{$this->getSecretAttribute()}]);
            return ClassHelper::callIfExists($this,'after_secret',$this->getId(),$result,$this->isSuccessful($result),null)
                ?? $result;
        } else {
            if($result = ClassHelper::callIfExists($this,'after_secret',$this->getId(),null,false,null)) {
                return $result;
            }
        }
        abort(404);

    }
}
