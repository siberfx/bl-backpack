<?php
/**
 * Created by PhpStorm.
 * User: darren
 * Date: 26/04/2020
 * Time: 00:30
 */

namespace Baseline\Backpack\Operations;


use Baseline\Backpack\Controller\CrudSecurity;
use Baseline\Backpack\Helpers\ClassHelper;
use Baseline\Backpack\Helpers\ControllerHelper;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

trait ToggleOperation
{
    use CrudSecurity;

    /**
     * @param $segment
     * @param $routeName
     * @param $controller
     */
    protected function setupToggleRoutes($segment, $routeName, $controller)
    {
        $config = ControllerHelper::getControllerConfigByBasename($routeName);
        Route::get($config['route'].'/{'.$routeName.'}/toggle', [
            'as'        => $routeName.'.toggle',
            'uses'      => $controller.'@toggle',
            'operation' => 'toggle',
        ]);
    }


    public function toggle() {
        // Check access
        $this->checkAccessOrFail();
        // get current entry
        //$object = $this->crud->getCurrentEntry();
        // this helps us avoid it attempting to rewrite fake fields
        $object = $this->getParentObject($this->basename);
        // get attribute to be toggled
        $attribute = request()->get('attribute');
        // is it a default type (have to unselect the others)
        if(request()->input('default_checkbox')) {
            foreach($this->crud->getEntries() as $entry) {
                if($entry[$attribute]) {
                    $entry[$attribute] = 0;
                    $entry->save();
                }
            }
        }
        // toggle it
        $object[$attribute] = $object[$attribute] ? false : true;
        // save it
        $object->save();
        // redirect
        ClassHelper::callIfExists($this,'before_toggle',$this->getId());
        try {
            $result = response()->redirectTo($this->crud->route);
        } catch (\Exception $e) {
            ClassHelper::callIfExists($this,'after_toggle',$this->getId(),null,false,$e);
            throw $e;
        }
        return ClassHelper::callIfExists($this,'after_toggle',$this->getId(),$result,$this->isSuccessful($result),null)
            ?? $result;
    }
}
