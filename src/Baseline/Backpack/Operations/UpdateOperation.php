<?php
/**
 * Created by PhpStorm.
 * User: darren
 * Date: 26/04/2020
 * Time: 00:30
 */

namespace Baseline\Backpack\Operations;

use Baseline\Backpack\Controller\BaselineCoreController;
use Baseline\Backpack\Controller\CrudSecurity;
use Baseline\Backpack\Helpers\ClassHelper;
use Baseline\Backpack\Helpers\ControllerHelper;
use Baseline\Backpack\Helpers\LogHelper;
use Illuminate\Support\Facades\Route;

trait UpdateOperation
{
    use CrudSecurity;
    use BaselineCoreController;

    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation {
        update as backpackUpdate;
        edit as backpackEdit;
    }

    /**
     * We need to override this method because it uses {id} as a parameter name, this doesn't work for nested parameters
     * Check this everytime for updated methods
     *
     * @param $segment
     * @param $routeName
     * @param $controller
     */
    protected function setupUpdateRoutes($segment, $routeName, $controller)
    {
        $config = ControllerHelper::getControllerConfigByBasename($routeName);
        Route::get($config['route'].'/{'.$routeName.'}/edit', [
            'as'        => $routeName.'.edit',
            'uses'      => $controller.'@edit',
            'operation' => 'update',
        ]);

        Route::put($config['route'].'/{'.$routeName.'}', [
            'as'        => $routeName.'.update',
            'uses'      => $controller.'@update',
            'operation' => 'update',
        ]);

        Route::get($config['route'].'/{'.$routeName.'}/translate/{lang}', [
            'as'        => $routeName.'.translateItem',
            'uses'      => $controller.'@translateItem',
            'operation' => 'update',
        ]);
    }

    public function edit($id) {
        // Check access
        $this->checkAccessOrFail();
        // Save the return address if we need to
        $this->saveReturnAddress($this->crud->getCurrentEntryId());
        // Stop the ui from printing "Back to list"
        $this->crud->denyAccess('list');
        // Prep the javascript helpers for the various fields/options
        $this->prepareJavascriptHelpers();
        // Set the heading
        if(!$this->crud->getHeading()) $this->crud->setHeading($this->crud->getCurrentEntry()->name);
        // Set the subheading
        if(!$this->crud->getSubHeading()) $this->crud->setSubHeading($this->subheading ?? 'Edit');
        // Set the breadcrumbs
        $this->data['breadcrumbs'] = $this->getBreadcrumbs();
        // Run the backpack edit
        ClassHelper::callIfExists($this,'before_edit',$id);
        try {
            $result = $this->backpackEdit($id);
        } catch (\Exception $e) {
            ClassHelper::callIfExists($this,'after_edit',$id,null,false,$e);
            throw $e;
        }
        return ClassHelper::callIfExists($this,'after_edit',$id,$result,true,null)
            ?? $result;
    }

    public function update() {
        // Check access
        $this->checkAccessOrFail();
        // Double check the id matches the route's id (route id is checked for security)
        if($this->getId() != request()->input('id')) {
            LogHelper::hack("Hack attempt, trying to edit an id [".request()->input('id')."] that differs from the route's id [".$this->getId()."]");
            abort(403);
        }
        // process transforms
        $this->processTransforms();
        // Encrypt the encrypted fields
        $this->prepareEncryptedFieldsForDB();
        // Add in the default fields
        $this->prepareDefaultFieldsForDB();
        // Add in the preset fields
        $this->preparePresetFieldsForDB();
        // Add missing or implied fields
        $this->prepareImpliedFieldsForDB();
        // Fix the confimed passwords which are stored in fake fields
        $this->fixConfirmedPasswordsInFakeFields();
        // fix encryptable for fake fields
        $this->applyEncryptableOnFakeFields();
        // Run the backpack update
        ClassHelper::callIfExists($this,'before_update',$this->getId());
        try {
            $result = $this->backpackUpdate();
        } catch (\Exception $e) {
            ClassHelper::callIfExists($this,'after_update',$this->getId(),null,false,$e);
            throw $e;
        }
        return ClassHelper::callIfExists($this,'after_update',$this->getId(),$result,$this->isSuccessful($result),null)
            ?? $result;
    }

}
