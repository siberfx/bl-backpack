<?php


namespace Baseline\Backpack\Responses;


class ErrorResponse extends ApiResponse
{
    public function __construct($message,$technical_message = null) {
        $this->json['errors'] = [
            [
                'message' => $message,
                'technical_message' => $technical_message ?? $message
            ]
        ];
    }
}
