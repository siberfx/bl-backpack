<?php


namespace Baseline\Backpack\Responses;


use Illuminate\Contracts\Support\Jsonable;

class ApiResponse implements Jsonable
{
    protected $json = [];

    public function toJson($options = 0)
    {
        $json = json_encode((object)array_filter($this->json));
        return $json;
    }

}
