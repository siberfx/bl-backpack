<?php


namespace Baseline\Backpack\Responses;


class SuccessResponse extends ApiResponse
{
    public function __construct() {
        $this->json['success'] = true;
    }
}
