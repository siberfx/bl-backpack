<?php
/**
 * Created by PhpStorm.
 * User: darren
 * Date: 24/04/2020
 * Time: 01:12
 */

namespace Baseline\Backpack\Controller;


use App\Helpers\ClassHelper;
use Baseline\Backpack\Helpers\ControllerHelper;
use Baseline\Backpack\Helpers\RouteHelper;
use Baseline\Backpack\Helpers\TenantHelper;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;


trait BaselineCoreController
{
    public $basename;
    public $global_access;
    public $dependency_fields;
    public $preset_fields = [];
    public $default_fields = [];
    public $subheading;
    public $transforms = [];

    /**
     * This method will build the crud, set entities and every other type of config for this controller
     *
     * @param null $baseroute (if you want the base route to be different from /admin)
     */
    function setupController()
    {
        // Get the config
        ControllerHelper::setCurrentController($this);
        $config = ControllerHelper::getControllerConfig($this);

        // Good stuff for our controller to know
        $this->basename = $config['basename'];
        $this->model = $config['model'];
        $this->global_access = $config['baseroute'] == 'global';
        // This is all the stuff required for the crud to work properly
        $this->crud->baseroute = $config['baseroute'];
        $this->crud->basename = $config['basename'];
        $this->crud->controller = $this;
        $this->crud->setModel($config['model']);
        $this->crud->setRoute(RouteHelper::parseRoute($config['full_route']));
        $this->crud->setEntityNameStrings($config['entity_name_strings'][0], $config['entity_name_strings'][1]);

        config(['backpack.base.scripts' => array_merge(config('backpack.base.scripts'), ['bl-backpack/js/core.js'])]);

        $this->runAdditionalSetups();
    }

    public function runAdditionalSetups() {
        preg_match_all('/(?<=^|;)setup([^;]+?)Controller(;|$)/', implode(';', get_class_methods($this)), $matches);

        if (count($matches[1])) {
            foreach ($matches[1] as $methodName) {
                $this->{'setup'.$methodName.'Controller'}();
            }
        }
    }

    public function getId()
    {
        return Route::current()->parameter($this->basename);
    }

    function getBreadcrumbs()
    {
        $parts = explode('/', request()->path());
        $count = 0;
        $breadcrumbs = [];
        $config = [];
        $last_config = [];
        foreach ($parts as $part) {
            // skip admin and global
            if ($count > 0) {
                if (((int)$part) > 0) {
                    // if its an id
                    if ($config) {
                        $method = null;
                        // only if there is a show or dashboard command
                        $class = $config['controller']['class'];
                        if (method_exists($class, 'show')) {
                            $method = 'show';
                        } elseif (method_exists($class, 'dashboard')) {
                            $method = 'dashboard';
                        }
                        $model = $config['model'];
                        // we hunt for the name, otherwise we default to the entity name
                        $breadcrumbs[$model::findOrFail($part)->name ??
                        $model::findOrFail($part)->username ??
                        $model::findOrFail($part)->basename ??
                        ucwords($config['entity_name_strings'][0])] = ($method ? url(config('backpack.base.route_prefix') . "/" . RouteHelper::parseRoute($config['route']) . "/$part/$method") : false);
                        $last_config = $config;
                    }
                } else {
                    $last_config = $config;
                    $config = ControllerHelper::getControllerConfigByBasename($part);
                    if ($config) {
                        $class = $config['controller']['class'];
                        if (method_exists($class, 'index')) {
                            $breadcrumbs[ucwords($config['entity_name_strings'][1])] = url(config('backpack.base.route_prefix') . "/" . RouteHelper::parseRoute($config['route']));
                        }
                    } else {
                        if ($part != 'dashboard') {
                            // If its a special case
                            //$breadcrumbs[ucfirst($part) . ($last_config ? " " . ucwords($last_config['entity_name_strings'][0]) : "")] = false;
                            $breadcrumbs[ucwords(preg_replace('/[_]/', ' ', $part))] = false;
                        } else {
                            $breadcrumbs[ucwords(preg_replace('/[_]/', ' ', $part))] = false;
                        }
                    }
                }
            }
            $count++;
        }
        return $breadcrumbs;
    }

    /**
     * Method adds the ids of the parent route parameters to the request for create/update
     */
    public function mergeParents()
    {
        // check tenant access
        $parameters = Route::current()->parameters();
        $merge = [];
        // Adds the tenant_id
        if ($this->global_access && TenantHelper::amISuperTenant()) {
            Log::info("Skipping overriding tenant allocation for route " . Route::currentRouteName() . ", global access is enabled and tenant is a super tenant");
        } else {
            //Log::debug("Merging in tenant foreign key " . config('tenant.foreign_key') . " -> " . session(config('tenant.foreign_key')));
            $merge = [config('tenant.foreign_key') => session(config('tenant.foreign_key'))];
        }
        // Adds all the parameters from the route
        foreach ($parameters as $parameter => $value) {
            if ($parameter != $this->basename) {
                //Log::debug("Merging $parameter => $value");
                $merge[$parameter . "_id"] = $value;
            }
        }
        // we need to add them as fields (or they don't make it into SQL)
        foreach ($merge as $key => $value) {
            $this->crud->addField(['type' => 'hidden', 'name' => $key]);
        }
        // then add them to the request
        request()->merge($merge);
    }

    /**
     * Method adds parent fields
     *
     */
    public function addParentFields()
    {
        $parameters = Route::current()->parameters();
        $fields = [];
        if ($this->global_access && TenantHelper::amISuperTenant()) {
            Log::info("Skipping adding tenant_id for " . Route::currentRouteName() . ", global access is enabled and tenant is a super tenant");
        } else {
            $this->crud->addField(['name' => config('tenant.foreign_key'), 'type' => 'hidden']);
        }
        // Adds all the parameters from the route
        foreach ($parameters as $parameter => $value) {
            if ($parameter != $this->basename) {
                $this->crud->addField(['name' => $parameter . "_id", 'type' => 'hidden']);
            }
        }


    }


    /**
     * Checks if a CRUD action is successful or not, helps with post processing of a CRUD request
     *
     * @param $return
     * @return int
     */
    protected function isSuccessful($return)
    {
        // 1 is for deletes, array is for json, redirect is for other crud
        return $return === "1" || is_array($return) || preg_match('/^HTTP\/1\.[0-9] 302 Found/', $return);
    }

    protected function getParentId($basename)
    {
        if (in_array($basename, Route::current()->parameterNames())) {
            $parent = Route::current()->parameter($basename);
            return $parent;
        } else {
            throw new \Exception('Failed to find parent of type ' . $basename);
        }
    }

    protected function getParentObject($basename)
    {
        $id = $this->getParentId($basename);
        $config = ControllerHelper::getControllerConfigByBasename($basename);
        if ($id && $config) {
            $model = $config['model'];
            return $model::findOrFail($id);
        } else {
            throw new \Exception('Failed to find parent of type ' . $basename);
        }
    }

    function removeInputIfBlank($input)
    {
        if (request()->input($input) === "") {
            $this->crud->getRequest()->request->remove($input);
        }
    }

    /**
     * This adds a field to the submitted fields that is preset (like an upstream client_id)
     *
     * @param $field_name string the name of the field
     * @param $value string the preset value of the field
     */
    protected function addPresetField($field_name, $value, $options = [])
    {
        $this->preset_fields[$field_name] = ['value' => $value, 'options' => $options];
    }

    /**
     * This adds the fields in before we process them into the db
     */
    protected function preparePresetFieldsForDB()
    {
        foreach ($this->preset_fields as $field => $data) {
            // this is required because we don't submit to SQL any fields which  aren't added to the field list
            $this->crud->addField(array_merge(['name' => $field, 'type' => 'hidden'], $data['options']));
            // This sets it in the request
            request()->merge([$field => $data['value']]);
        }
        //dd($this->crud->fields());
    }

    /*
     * Call this in the store/update to add the field and add the value to the request
     */
    protected function addJustInTimeField($field,$value,$other_attributes = [])
    {
        // this is required because we don't submit to SQL any fields which  aren't added to the field list
        $this->crud->addField(array_merge($other_attributes,['name' => $field, 'type' => 'hidden', 'value' => $value]));
        // This sets it in the request
        request()->merge([$field => $value]);
    }

    protected function prepareImpliedFieldsForDB()
    {
        foreach ($this->crud->fields() as $name => $field) {
            // prefix ranges need additional fiels for submission
            if (isset($field['type']) && $field['type'] == 'prefix_range') {
                $this->crud->addField(['name' => $field['start_name'], 'type' => 'hidden']);
                $this->crud->addField(['name' => $field['finish_name'], 'type' => 'hidden']);
            }
        }
    }

    /**
     * Sets up the dependency field
     * @param $field_array
     */
    protected function setDependencyFields($field_array)
    {
        $this->dependency_fields = $field_array;
    }

    /**
     * Add a field which behaves like a default, meaning if its not set, it must be, if it is checked, then all others must be unchecked.
     */
    protected function addDefaultField($field, $label, $hint = "")
    {
        // this part handles adding the field
        $entries = $this->crud->getEntries();
        if (!count($entries)) {
            $this->addPresetField($field, 1);
        } else {
            $this->crud->addField(['name' => $field, 'label' => $label, 'type' => 'checkbox', 'hint' => $hint]);
        }
        // we need to make a note so we can unset others if this is set
        $this->default_fields[] = $field;
    }

    /*
     * This method is for 1 way hashing of a password.  set encrypt on the field and we will hash it irreversably
     */
    protected function prepareEncryptedFieldsForDB() {
        foreach ($this->crud->fields() as $name => $field) {
            if(isset($field['encrypt']) && $field['encrypt']) {
                // check if they are equal (if they are, we need to equalise the hash as well)
                $original_value = request()->input($field['name']);
                request()->merge([$field['name'] => Hash::make($original_value)]);
                if($field['type'] == 'confirmed_password') {
                    if($original_value == request()->input($field['name']."_confirmation")) {
                        // hashes have to be the same because passwords are the same
                        request()->merge([$field['name']."_confirmation" => request()->input($field['name'])]);
                    } else {
                        // hashes will be different but passwords were as well
                        request()->merge([$field['name']."_confirmation" => Hash::make(request()->input($field['name']."_confirmation"))]);
                    }
                } else {
                }
            }
        }
    }

    protected function prepareDefaultFieldsForDB()
    {
        $model = $this->crud->model;
        foreach ($this->default_fields as $field) {
            if (request()->input($field)) {
                // ok its set, we need to devalue all the others
                $entries = $this->crud->getEntries();
                foreach ($entries as $entry) {
                    if ($entry[$field]) {
                        // need to do it like this because we have fake fields
                        $model::where('id', $entry->id)->update([$field => 0]);
                    }
                }
            }
        }
    }

    /**
     * This method runs from the create and edit methods of the various operations to add the javascript assists when required
     */
    protected function prepareJavascriptHelpers()
    {
        // field dependency helper
        if ($this->dependency_fields) {
            $this->crud->scriptlets[] = view('baseline.js.field_management', ["fields" => $this->dependency_fields]);
        }
        // If we have a confimed_password field, then we need this view to manage it.
        $confirmed_password_fields = [];
        //dd($this->crud->fields());
        foreach ($this->crud->fields() as $name => $field) {
            if (isset($field['type']) && $field['type'] == 'confirmed_password') {
                $confirmed_password_fields[] = $field['name'];
            }
        }
        if ($confirmed_password_fields) {
            $this->crud->scriptlets[] = view('baseline.js.confirmed_password_check', ['fields' => $confirmed_password_fields]);
        }
    }

    protected function addTransform($field,$callback) {
        $this->transforms[$field][] = $callback;
    }

    protected function processTransforms() {
        foreach($this->transforms as $field => $callbacks) {
            foreach($callbacks as $callback) {
                request()->merge([$field => $callback(request()->input($field))]);
            }
        }
    }

    protected function setSubHeading($subheading)
    {
        $this->subheading = $subheading;
    }

    private function fixFakeListsFromRepeatableField($new_field, $repeatable_field)
    {
        request()->merge([$new_field => json_decode(request()->input($repeatable_field), true)]);
    }

    public function convertBase64ImageToResponse($image_data, $width = null,$height = null)
    {
        $data = explode(',', $image_data);
        $content = base64_decode($data[1]);
        $meta = [];
        if (preg_match('/data:(.*);base64/', $data[0], $meta)) {
            $types = explode('/', $meta[1]);
            $img = Image::make($image_data);
            if($width && $height) {
                $img->resize($width,$height);
            } else if($width || $height) {
                $img->resize($width,$height, function(Constraint $constraint) {
                    $constraint->aspectRatio();
                });
            }
            $response = $img->response()
                //response(base64_decode($data[1]))
                ->setStatusCode(200)
                //->header('Content-Type', $meta[1])
                ->header('Content-disposition', 'attachment; filename="avatar.' . $types[1] . '"');
            return $response;
        }
        abort(404);
    }

    // this method checks the model and if it has encryptable on it, it looks through for fake fields to encrypt.
    protected function applyEncryptableOnFakeFields()
    {
        $model = $this->crud->model;
        if (in_array('Baseline\Backpack\Model\Traits\Encryptable', array_keys(class_uses($model)))
            && property_exists($model, 'encryptable')) {
            $obj = new $model();
            $encryptable = ClassHelper::accessProtected($obj, 'encryptable');
            if ($encryptable) {
                $fields = $this->crud->fields();
                foreach ($encryptable as $encrypt_me) {
                    $value = request()->input($encrypt_me);
                    if ($value) {
                        $field = $fields[$encrypt_me] ?? null;
                        if ($field && isset($field['fake']) && $field['fake']) {
                            // this encryptable field is fake
                            Log::debug("We will encrypt fake field $encrypt_me manually before passing to backpack to store");
                            $encrypted_value = Crypt::encrypt($value);
                            request()->merge([$encrypt_me => $encrypted_value]);
                            // if its a confirmed password field, we also need to encrypt the confirmed value if the values match
                            if (isset($field['type']) && $field['type'] == 'confirmed_password' && request()->input($encrypt_me."_confirmation") == $value) {
                                request()->merge([$encrypt_me."_confirmation" => $encrypted_value]);
                            }
                        }
                    }
                }
            }
        }
    }


    protected function fixConfirmedPasswordsInFakeFields()
    {
        foreach ($this->crud->fields() as $name => $field) {
            if (isset($field['type']) && $field['type'] == 'confirmed_password' && isset($field['fake']) && $field['fake'] && !request()->input('change_' . $name)) {
                // ok, so we have a fake field which stores a password, yet the user didn't tick change.
                // if we don't fix this field, it will not be updated in the DB with the update of its containing field
                $entry = $this->crud->getCurrentEntry();
                // change the request so it has its current db value for re-insertion into the DB.
                request()->merge([$name => $entry->$name, $name . "_confirmation" => $entry->$name]);
                Log::debug("Fixed $name value as a fake field based confirmed password");
            }
        }
    }

    protected function saveReturnAddress($current_id = 'create', $input_name = 'return_address') {
        if($return = request()->input($input_name)) {
            session([$this->basename.'.'.$current_id.'.return_address' => $return]);
        }
    }

    protected function getReturnAddress($current_id = 'create') {
        if($return = request()->session()->pull($this->basename.'.'.$current_id.'.return_address')) {
            return $return;
        }
        return null;
    }
}
