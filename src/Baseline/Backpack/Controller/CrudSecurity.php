<?php
/**
 * Created by PhpStorm.
 * User: darren
 * Date: 25/04/2020
 * Time: 23:35
 */

namespace Baseline\Backpack\Controller;


use Baseline\Backpack\Helpers\ControllerHelper;
use Baseline\Backpack\Helpers\TenantHelper;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

trait CrudSecurity
{
    public $ignore_parents = [];

    public function ignoreParentsWithEntries($controller,$parent_name) {
        $controller->ignore_parents[$controller->basename][$parent_name] = 1;
    }

    /**
     * Adds the required SQL clauses to ensure that the scope is defined according to partner and route parameters
     *
     * @param $controller The controller running the route
     */
    public function limitSearchAndEntries($controller)
    {
        $wheres = [];
        // If we are requesting global access and we are the super partner, we don't apply the partner_id limit
        if ($controller->global_access && TenantHelper::amISuperTenant()) {
            Log::info("Skipping tenant check for this route " . Route::currentRouteName() . ", global access is enabled and tenant is a super tenant");
        } else {
            // in every other circumstance we do (SECURITY!!)
            //$controller->crud->addClause('where', $controller->crud->model->getTable().'.'.(config('tenant.foreign_key')), session(config('tenant.foreign_key')));
            $wheres[$controller->crud->model->getTable().'.'.(config('tenant.foreign_key'))] = session(config('tenant.foreign_key'));
        }
        $parameters = Route::current()->parameters();
        // Make sure all parameters are in the where clause
        //Log::debug(print_r($controller->ignore_parents,true));
        foreach ($parameters as $param => $value) {
            if ($controller->basename != $param) {
                //$controller->crud->addClause('where', $param . '_id', $value);
                if($controller->ignore_parents[$controller->basename][$param] ?? null) {
                    //Log::warning("We are not adding parent $param to security where clause because it is listed as being ignored on $controller->basename");
                } else {
                    //Log::debug("Adding $controller->basename - ".$param." ".(debug_backtrace()[1]['function']));
                    $wheres[$param . '_id'] = $value;
                }
            }
        }
        if($wheres) {
            $controller->crud->addClause('where',function($query) use ($wheres) {
                foreach($wheres as $field => $value) {
                    $query->where($field,$value);
                }
            });
        }
    }

    /**
     * checks if the current user has access to the route.
     */
    public function checkAccessOrFail()
    {
        // First check he has access to this partner
        $user = backpack_user();
        if (!$user) {
            Log::error("Failed to find user, denying access");
            abort(403);
        }
        // currently the user's partner id has to match his session partner_id
        if (!TenantHelper::userAccessToCurrentTenant($user)) {
            Log::error("User $user->email doesn't have access to tenant " . TenantHelper::currentTenantId() . ", denying access");
            abort(403);
        }
        // The partner has to be valid
        if(TenantHelper::currentTenantId()) {
            if (!TenantHelper::isValidTenant(TenantHelper::currentTenantId())) {
                Log::error("User $user->email cannot proceed to tenant " . TenantHelper::currentTenantId() . " as they are invalid, denying access");
                abort(403);
            }
        }
        // If he is a super user accessing the global baseroute, he is good
        if(preg_match('%^'.config('tenant.global_baseroute').'/%',request()->path())) {
            if(TenantHelper::amISuperTenant()) {
                return true;
            } else {
                Log::error("User $user->email is attempting to access the global area but he is not a super tenant");
                abort(403);
            }
        }

        // Checks whether current partner has access to the chain of objects
        // one at a time
        $known_basenames = collect(ControllerHelper::getControllerConfigs())->pluck('basename')->toArray();
        $parameters = [];
        foreach(Route::current()->parameters() as $key => $value) {
            if(in_array($key,$known_basenames)) {
                $parameters[$key] = $value;
            }
        }
        //Log::debug("Checking ".$user->email." permissions on ".request()->path().", ". count($parameters) . " parameters . ".json_encode($parameters));
        try {
            // If there are parameters to check
            if (count($parameters) > 0) {
                foreach ($parameters as $parameter => $value) {
                    //Log::debug("Checking user " . $user->email . ", tenant " . TenantHelper::currentTenantId() . " has access to $parameter -> " . $value);
                    // Get the model of the parameter
                    $modelType = ControllerHelper::getControllerConfigByBasename($parameter)['model'];
                    //Log::debug("Trying to access $modelType");
                    // Accessing of the partner model is forbidden
                    //      unless its under the global baseroute and you're the super partner
                    //      or its your own partner
                    if ($modelType == config('tenant.model') && !preg_match('|^'.config('tenant.global_baseroute').'/|', request()->path())) {
                        // trying to access another partner, no
                        if ($value != TenantHelper::currentTenantId()) {
                            Log::error("Access is denied, user " . $user->email . " is trying to access the partner model of $value, when its bound to ".TenantHelper::currentTenantId());
                            abort(403);
                        }
                        $model = new $modelType();
                        // If this parameter doesn't exist
                        if (!$model->where("id", $value)->first()) {
                            Log::error("Access is denied, user " . $user->email . " is not allowed to access $parameter $value, it doesn't seem to exist");
                            abort(403);
                        }

                    } else {
                        // otherwise, we check our session's partner id matches the parameter we are trying to access.
                        if (!TenantHelper::checkCurrentTenantOwns($value,$modelType)) {
                            Log::error("Access is denied, user " . $user->email . " is not allowed to access $parameter $value, the tenant doesn't own that $parameter");
                            abort(403);
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            Log::error("Failed to check permissions for " . $user->email . " on route " . request()->path() . " : " . $e->getMessage());
            abort(403);
        }
    }

}
