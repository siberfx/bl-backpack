<?php
/**
 * Created by PhpStorm.
 * User: darren
 * Date: 04/12/2018
 * Time: 02:56
 */

namespace Baseline\Backpack\Model\Traits;


use Baseline\Backpack\Helpers\LogHelper;
use Baseline\Backpack\Helpers\TenantHelper;
use Illuminate\Support\Facades\Log;

trait ProtectAttribute
{
    /**
     * Please remember to add the attributes that need to be protected from update
     * protected $noupdate = [ 'attribute_name_1' ];
     */

     public static function bootProtectAttribute() {

         static::updating(function($obj) {

             if(isset($obj->noupdate)) {
                 foreach($obj->noupdate as $field) {
                     // we cater for don't allow any updates at all
                     if($field == '*') {
                         foreach($obj->fillable as $attribute) {
                             self::removeIfNecessary($obj,$attribute);
                         }
                     } else {
                         self::removeIfNecessary($obj, $field);
                     }
                 }
             }
         });

     }

    /**
     * @param $obj
     * @param $field
     */
    static function removeIfNecessary($obj, $field): void
    {
        // if its set, is dirty or being set from null/empty to something (i.e. will be updated)
        if (isset($obj->attributes[$field]) && $obj->isDirty($field) && $obj->getOriginal($field)) {
            if(app()->runningInConsole()) {
                Log::debug("$field of " . get_class($obj) . " is a protected field, but console is allowed to update from '" . $obj->getOriginal($field) . "' => '" . $obj->attributes[$field] . "' for id [$obj->id]");
            } elseif (TenantHelper::amISuperTenant()) {
                Log::debug("$field of " . get_class($obj) . " is a protected field, but super partner id=".config('unity.super_partner')." is allowed to update from '" . $obj->getOriginal($field) . "' => '" . $obj->attributes[$field] . "' for id [$obj->id]");
            } else {
                LogHelper::hack("Refusing to update $field: '" . $obj->getOriginal($field) . "' => '" . $obj->attributes[$field] . "' of " . get_class($obj) . ' [' . $obj->id . '], this is a protected field.  This is either a HACK ATTEMPT or a bug, please investigate.');
                // revert it back to original
                $obj->syncOriginalAttribute($field);
            }
        }
    }

}
