<?php
/**
 * Created by PhpStorm.
 * User: darren
 * Date: 2016-08-21
 * Time: 09:40 AM
 */

namespace Baseline\Backpack\Model\Traits;

use Illuminate\Database\Eloquent\Model;

trait LastDirty
{
    public $lastDirty = [];

    public static function bootLastDirty() {
        static::updating(function (Model $obj) {
            $obj->lastDirty = $obj->getDirty();
        });
    }
}
