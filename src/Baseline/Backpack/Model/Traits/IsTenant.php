<?php


namespace Baseline\Backpack\Model\Traits;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Log;

trait IsTenant
{


    public static function bootIsTenant() {
        // Adds the relation dynamically
        Builder::macro(config('tenant.relation'), function () {
            return $this->getModel()->belongsTo(config('tenant.model'),config('tenant.foreign_key'));
        });
    }

    // updates fillable with the tenant id foreign key
    public function __construct(array $attributes = [])
    {
        $this->addTenantIdToFillable();
        parent::__construct($attributes);
    }

    public function addTenantIdToFillable()
    {
        if (!in_array(config('tenant.foreign_key'), $this->fillable)) {
            $this->fillable[] = config('tenant.foreign_key');
        }
    }
    public function getAttribute($key) {
        $method_name = config('tenant.relation');
        if($key == $method_name) {
            return $this->$method_name()->first();
        }
        return parent::getAttribute($key);
    }

}
