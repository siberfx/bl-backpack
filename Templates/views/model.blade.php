{!! '<?php' !!}

namespace App\Models;

use Baseline\Backpack\Model\Traits\ProtectAttribute;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Baseline\Backpack\Model\Traits\IsTenant;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class {{ $model }} extends Model
{
    use CrudTrait,
        Notifiable,
        ProtectAttribute,
        IsTenant;

    protected $table = '{{ $table }}';
    protected $primaryKey = 'id';
    public $timestamps = true;
    protected $hidden = [ "id" ];
@if($fields ?? null)
    // uncomment this to review the attribute which should be used to "name" the record in a human readable way
    //protected $indicatorAttribute = '{{ $fields[0] }}';
@endif
    protected $encryptable = [
@foreach($fields ?? [] as $field)
@if($field == 'password')
        '{{ $field }}',
@endif
@endforeach
    ];
    protected $fillable = [
@foreach($fields ?? [] as $field)
        "{{ $field }}",
@endforeach
@foreach($ancestors ?? [] as $ancestor)
        "{{ $ancestor }}_id",
@endforeach
    ];

    protected $noupdate = [ '{{ $tenant['foreign_key'] }}' ];

    protected static function boot()
    {
        parent::boot();
    }

@foreach($ancestors ?? [] as $ancestor)
    public function {{ \Baseline\Backpack\Helpers\MiscHelper::underscoresToCamelCase($ancestor) }}() {
        return $this->belongsTo('App\Models\{{ \Baseline\Backpack\Helpers\MiscHelper::underscoresToCamelCase($ancestor,true) }}');
    }
@endforeach
}
