{!! '<?php' !!}

namespace App\Http\Requests\Admin;

use Baseline\Backpack\Helpers\RouteHelper;
use Illuminate\Support\Str;

class {{ $model }}Request extends \Illuminate\Foundation\Http\FormRequest
{
    public function authorize()
    {
        return backpack_auth()->check();
    }

    public function rules(){
        $rules = [];
        $this->addCommonRules($rules);
        if(RouteHelper::is_create()) {
            $this->addCreateRules($rules);
        }
        if(RouteHelper::is_update()) {
            $this->addUpdateRules($rules);
        }
        return $rules;
    }

    public function addCommonRules(&$rules) {
@foreach($fields ?? [] as $field)
@if($field == 'enabled')
        $rules['{{ $field }}'] = 'required|boolean';
@elseif(Str::contains(strtolower($field),'phone'))
        $rules['{{ $field }}'] = 'required|phone:AUTO,'.implode(',',config('bl-backpack.validation.numbers_supported',[])).'|max:255';
@elseif(Str::contains(strtolower($field),'mail'))
        $rules['{{ $field }}'] = 'required|email:rfc,dns';
@else
        $rules['{{ $field }}'] = 'required|max:255';
@endif
@endforeach
    }

    public function addCreateRules(&$rules) {
        //$rules['basename'] = 'required|valid_basename|unique:form,basename,'.request('id','NULL').',id,partner_id,'.PartnerHelper::currentPartnerId();
    }

    public function addUpdateRules(&$rules) {

    }

    public function messages()
    {
        return [
@foreach($fields ?? [] as $field)
    @if(Str::contains(strtolower($field),'phone'))
        '{{ $field }}.phone' => 'Please enter a valid phone number',
    @endif
@endforeach
        ];
    }
}
