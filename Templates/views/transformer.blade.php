{!! '<?php' !!}

namespace {{ $namespace }};


use App\Models\{{ $model }};
use Baseline\Backpack\Transformers\BaseTransformer;

class {{ $class }} extends BaseTransformer
{
    public function transform({{ $model }} ${{ lcfirst($model) }}) {
        // smap maps all attributes mentioned in the array to the outbound fractal
        return $this->smap({!! json_encode($fields ?? []) !!},${{ lcfirst($model) }}, false);
    }
}
