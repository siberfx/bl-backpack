<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEnabledToUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable(config('tenant.relation'))) {
            Schema::create(config('tenant.relation'), function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name');
                $table->string('basename')->unique();
                $table->boolean('enabled')->default(true);
                $table->timestamps();
            });
        }

        Schema::table('users', function (Blueprint $table) {
            if(!Schema::hasColumn('users',config('tenant.foreign_key'))) {
                $table->bigInteger(config('tenant.foreign_key'))->after('id');
            }
            if(!Schema::hasColumn('users','enabled')) {
                $table->boolean('enabled')->default(true);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('enabled');
        });
    }
}
