<?php
/**
 * Created by PhpStorm.
 * User: darren
 * Date: 28/04/2020
 * Time: 00:02
 */

namespace App\Http\Controllers\Admin;


use Backpack\CRUD\app\Http\Controllers\CrudController;
use Baseline\Backpack\Controller\BaselineCoreController;
use Baseline\Backpack\Helpers\FilterHelper;
use Baseline\Backpack\Operations\CreateOperation;
use Baseline\Backpack\Operations\DashboardOperation;
use Baseline\Backpack\Operations\DeleteOperation;
use Baseline\Backpack\Operations\ListOperation;
use Baseline\Backpack\Operations\SecretOperation;
use Baseline\Backpack\Operations\ToggleOperation;
use Baseline\Backpack\Operations\UpdateOperation;
use Illuminate\Support\Facades\Log;

class TemplateCrudController extends CrudController
{
    use BaselineCoreController;

    // For list/index (add { index as baselineIndex; } if required to create index)
    use ListOperation;
    // for dashboard (you WILL need a dashboard method)
    use DashboardOperation;
    // for edit/update (add { update as baselineUpdate; } if required to create update)
    use UpdateOperation;
    // for create/store (add { store as baselineStore; } if required to create store)
    use CreateOperation;
    // for deletion (add { destroy as baselineDestroy; } if required to create destroy)
    use DeleteOperation;
    // for the toggle function (add { toggle as baselineToggle; } if required to create toggle)
    use ToggleOperation;
    // for the secret function (check below to set the attribute that holds the secret)
    use SecretOperation;

    public static $crud_config = [
        'model' => 'App\Models\*****',
        'entity_name_strings' => ['**template**', '**templates**'],
        'basename' => '**basename**',
        'parent' => '**parent_basename**', // optional
        //'baseroute' => 'global', // only for global baseroutes
    ];

    public function setup()
    {
        // Sets up the controller
        $this->setupController();
        // Any other setup thats not operation specific
    }

    protected function setupListOperation()
    {
        // Columns
        //$this->crud->addColumn(['name' => 'field_name', 'label' => 'Nice Label', 'type' => 'text']);
        //$this->crud->addColumn(['name' => 'enabled', 'label' => 'Enabled', 'type' => 'active_checkbox']);
        // Filters
        /*
        FilterHelper::addEnabledFilter($this->crud,"Enabled","enabled");
        $this->crud->addFilter(['type' => 'simple', 'name' => 'enabled_clients', 'label' => 'Enabled Clients'],
            false,
            function () {
                $this->crud->addClause('where', 'enabled', 1);
            });
        */
        // Add line buttons

        // set default order
        if (!request()->has('order')) { $this->crud->orderBy('name'); }
    }

    /**
     * This method preps the create options, setup validation, add your fields and other functionality prior to create / store method
     */
    protected function setupCreateOperation()
    {
        // Setup validation
        $this->crud->setValidation(StoreClientRequest::class);
        // Setup fields
        $this->setupCommonFields(); // this is for fields common to both update and create
        // Fields for create only
        $this->crud->addField(['name' => 'field_name', 'label' => 'Nice Label', 'type' => 'text', 'hint' => 'Instruction Text']);
        // Setup dependency fields (optional)
        //$this->setDependencyFields($this->getDependencyFields());
        // Set the save action (optional)
        // This redirects to a dashboard
        $this->crud->replaceSaveActions([
            'name' => 'Save',
            'redirect' => function($crud, $request, $itemId) {
                // return to the list
                return $this->crud->route;
                // return to a dashboard
                //return $this->crud->route . "/" . $this->crud->entry->id . "/dashboard";
            }
        ]);
        // Setup other stuff for the create page
        //$this->crud->stylesheets[] = '*****/*****.css';
        //$this->crud->scriptlets[] = 'console.log("Hi there")';
        //$this->crud->scripts[] = '**/**.js';
    }

    /**
     * This method allows you to setup the validation, fields and other stuff for edit/update
     */
    protected function setupUpdateOperation()
    {
        // Setup validation
        $this->crud->setValidation(UpdateClientRequest::class);
        // Setup fields
        $this->setupCommonFields(); // this is for fields common to both update and create
        // Update specific fields
        $this->crud->addField(['name' => 'field_name', 'label' => 'Nice Label', 'type' => 'text', 'hint' => 'Instruction Text']);
        // Setup dependency fields (optional)
        //$this->setDependencyFields($this->getDependencyFields());
        // Set the save action (optional)
        // This redirects to a dashboard
        $this->crud->replaceSaveActions([
            'name' => 'Save',
            'redirect' => function($crud, $request, $itemId) {
                // return to the list
                return $this->crud->route;
                // return to a dashboard
                //return $this->crud->route . "/" . $request->input('id') . "/dashboard";
            }
        ]);
        // Setup other stuff for the create page
        //$this->crud->stylesheets[] = '*****/*****.css';
        //$this->crud->scriptlets[] = 'console.log("Hi there")';
        //$this->crud->scripts[] = '**/**.js';
    }

    /**
     * This method allows you to setup common fields for update and create
     */
    private function setupCommonFields()
    {
        //$this->crud->addField(['name' => 'name', 'label' => 'Client Name', 'hint' => 'The name of the client or business name']);
        //$this->crud->addField(['name' => 'enabled', 'label' => 'Enabled', 'hint' => 'Is this enabled?', 'default' => 1]);
    }

    // If you have dependency fields, you can define them here and link them into create and update
    /*
    private function getDependencyFields() {
        return [
            // field name
            'applies_to' => [
                // switch/case values
                'site' => [
                    // Change this field or exec
                    'site_id' => ['disabled' => false, 'parent_visible' => true ],
                ],
                '*' => [
                    // Change this field or exec
                    'site_id' => ['disabled' => true, 'parent_visible' => false ],
                ],
            ],
            // field name
            'service_id' => [
                // switch/case values
                '*' => [
                    // Change this field or exec
                    'documentation' => [ 'function' => 'updateInfoFromTemplate($(this).val());' ],
                ],
            ],
        ];
    }
    */


    /** IF you have a secretOperation
     * What attribute on the model holds the secret
     */
    protected function setupSecretOperation() {
        $this->setSecretAttribute('passphrase');
    }


    // Need a new route, you can add it by using setup*****Routes as below
    /*
    protected function setupStatusRoutes($segment, $routeName, $controller)
    {
        $config = ControllerHelper::getControllerConfigByBasename($routeName);
        Route::get($config['route'] . '/{' . $routeName . '}/status', [
            'as' => $routeName . '.status',
            'uses' => $controller . '@status',
            'operation' => 'status',
        ]);
    }

    // Now this is the method which will be called for the route above
    public function status()
    {
        // You need this to make sure this route is accessible by this user
        $this->checkAccessOrFail();
        // return some view?
    }
    */


    /**
     * This is the dashboard view for the more complex objects to show more detail
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function dashboard()
    {
        // Setup some stuff

        //$this->crud->stylesheets[] = '*****/*****.css';
        //$this->crud->scriptlets[] = 'console.log("Hi there")';
        //$this->crud->scripts[] = '**/**.js';

        // return your dashboard view
        return view('****.****.dashboard', $this->data);
    }

    /**
     * This method is needed only if you want to do stuff after a store
     */
    public function store()
    {
        // Call the upstream store command (this requires [ store as baselineStore; } when declaring the StoreOperation Trait
        $result = $this->baselineStore();
        // Check result
        if($this->isSuccessful($result)) {
            // Do something if successful
        } else {
            Log::error("Failed to store");
        }
        return $result;
    }

    /**
     * This method is needed only if you want to do stuff after an update
     */
    public function update()
    {
        // Call the upstream update command (this requires [ store as baselineUpdate; } when declaring the UpdateOperation Trait
        $result = $this->baselineUpdate();
        // Check result
        if($this->isSuccessful($result)) {
            // Do something if successful
        } else {
            Log::error("Failed to update");
        }
        return $result;
    }

}
